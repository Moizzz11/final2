<?php
class PaperM extends CI_Model
{

	function ViewAllPapers()
	{
		$ConfID = $this->session->userdata['LoggedIn']['ConfID'];
      
		$this->db->select('Abstract');
		$this->db->from('file');
		$this->db->where('ConfID',$ConfID);

		$query = $this->db->get();
		return $query->result();
		//return $query->row()->Abstract;
	}

	function ViewAllRevs()
	{
		$ConfID = $this->session->userdata['LoggedIn']['ConfID'];
      
		$this->db->select('Name');
		$this->db->from('reviewer');
		$this->db->where('ConfID',$ConfID);

		$query = $this->db->get();
		return $query->result();
	} 

	function ViewPapers()
	{
		$ConfID = $this->session->userdata['LoggedIn']['ConfID'];
      
		$this->db->select('FileID,FileName,Abstract,Track');
		$this->db->from('file');
		$this->db->where('ConfID',$ConfID);

		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			foreach($query->result() as $row)
			{
				$data[] = $row;
			}

			return $data;
		}

		else
		{
			return false;
		}

	}

	function RetTrackName($TrackID)
	{
		//$ComHeadEmail = $this->session->userdata['LoggedIn']['Email'];
		$ConfID = $this->session->userdata['LoggedIn']['ConfID'];

		$this->db->select('TrackName');
		$this->db->from('tracks');
		$this->db->where('ConfID',$ConfID);
		$this->db->where('TrackID',$TrackID);

		$query = $this->db->get();
		return $query->row()->TrackName;
	}

}

?>