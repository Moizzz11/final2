<?php
class AdminAllConM extends CI_Model
{
	/*function getAll(){
	$query = $this->db->query("SELECT * FROM conference");

	return $query->result();
	}
	*/

	function DeleteConf($ConfID)
	{
		$this->db->delete('Conference', array('ConfID' => $ConfID)); 

		// Produces:
		// DELETE FROM mytable 
		// WHERE id = $id
	}

	function ViewAllConf()
	{
		$this->db->select('ConfID,ConfLName,ConfSName,DueDate,ConfSub');
		$this->db->from('conference');

		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			foreach($query->result() as $row)
			{
				$data[] = $row;
			}

			return $data;
		}

		else
		{
			return false;
		}
	}

}

?>