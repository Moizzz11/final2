<?php
class AddRevM extends CI_Model
{

	function get_random_password($chars_min=6, $chars_max=8, $use_upper_case=false, $include_numbers=false, $include_special_chars=false)
    {
            //parent::CI_Controller();
            $length = rand($chars_min, $chars_max);
            $selection = 'aeuoyibcdfghjklmnpqrstvwxz';
            if($include_numbers) {
                $selection .= "1234567890";
            }
            if($include_special_chars) {
                $selection .= "!@\"#$%&[]{}?|";
            }

            $password = "";
            for($i=0; $i<$length; $i++) {
                $current_letter = $use_upper_case ? (rand(0,1) ? strtoupper($selection[(rand() % strlen($selection))]) : $selection[(rand() % strlen($selection))]) : $selection[(rand() % strlen($selection))];            
                $password .=  $current_letter;
            }                

            return $password;
    }

	function AddRev()
	{

		$Email = $this->input->post('Email');
		$ConfID = $this->session->userdata['LoggedIn']['ConfID'];
		$PW = $this->get_random_password();

		$data = array('Email'=>$Email, 'ConfID'=>$ConfID, 'Password'=>$PW);
		$this->db->insert('reviewer',$data);

		$cid = $this->db->insert_id();
		$data2 = array('Email'=>$Email, 'Password'=>$PW, 'ConfID'=>$ConfID, 'Type'=>'R', 'ID'=>$cid);
		$this->db->insert('login', $data2);

	}

	function AddConf()
	{
		$ConfLongName = $this->input->post('ConfLongName');
        $ConfShortName = $this->input->post('ConfShortName');
        $SubDate = $this->input->post('SubDate');
        //$Tracks = $this->input->post('Tracks');
        $Subject = $this->input->post('Subject');

        $data = array('ConfLName'=>$ConfLongName, 'ConfSName'=>$ConfShortName, 'DueDate'=>$SubDate, 'ConfSub'=>$Subject);
        $this->db->insert('conference',$data);
        $cid = $this->db->insert_id();
        return $cid;

	}

	function ViewRev()
	{
		$session_data = $this->session->userdata('LoggedIn');
        $data1=array();
        $data1['ConfID'] = $session_data['ConfID']; 
        $temp = $data1['ConfID'];
      
		$this->db->select('RID,Name,Email,Spec');
		$this->db->from('reviewer');
		$this->db->where(array('ConfID'=> $temp));

		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			foreach($query->result() as $row)
			{
				$data[] = $row;
			}

			return $data;
		}

		else
		{
			return false;
		}
	}

	function ViewRevOnly()
	{

		$session_data = $this->session->userdata('LoggedIn');
        $data2=array();
        $data2['Email'] = $session_data['Email']; 
        $temp2 = $data2['Email'];

		$this->db->select('Name,Email,Spec');
		$this->db->from('reviewer');
		$this->db->where(array('Email'=> $temp2));

		$query = $this->db->get();
		
		if($query->num_rows()>0)
		{
			foreach($query->result() as $row)
			{
				$data[] = $row;
			}

			return $data;
		}

		else
		{
			return false;
		}
	}

	function ModRev()
	{

		$session_data = $this->session->userdata('LoggedIn');
        $data2 = array();
        $data2['Email'] = $session_data['Email']; 
        $temp2 = $data2['Email'];

		$Name = $this->input->post('Name');
		$Spec = $this->input->post('spec');

		$data = array('Name'=>$Name, 'Spec'=>$Spec);
		$this->db->where(array('Email'=>$temp2));
		$this->db->update('reviewer', $data);


	}

	function AddChair()
	{
		$ConfID = $this->input->post('ConfID');
		$ConfChairName = $this->input->post('ConfChairName');
		$ConfChairEmail = $this->input->post('ConfChairEmail');
		$PW = $this->get_random_password();

		$data = array('ChairName'=>$ConfChairName, 'ConfID'=>$ConfID, 'ChairEmail'=>$ConfChairEmail, 'ChairPW'=>$PW);
		$return = $this->db->insert('chairperson',$data);
		$cid = $this->db->insert_id();
		$data2 = array('Email'=>$ConfChairEmail,'Password'=>$PW, 'ConfID'=>$ConfID, 'Type'=>'CP', 'ID'=>$cid);

		

		if (!$return) 
		{
	      $msg = $this->db->_error_message();
	      $num = $this->db->_error_number();
	      //log_message('error', "DB Error: (".$ret['ErrorNumber'].") ".$ret['ErrorMessage']);

	      $data['msg'] = "Error(".$num.") ".$msg;
	      echo '<div class="alert alert-info" <a class="close pull-right" data-dismiss="alert"></a>The entered conference does not exist!</div>';
	   
	      return false;
    	} 

    	else 
    	{ 
			$this->db->insert('login', $data2);
			return true;
		}



	}

	function CheckChairExist()
	{
		$ConfID = $this->input->post('ConfID');

		$this->db->select('ConfID');
		$this->db->from('chairperson');
		$this->db->where('ConfID',$ConfID);

		$query = $this->db->get();

		if ($query->num_rows()>0)
		{
        	return false;
    	}

    	else
    	{
        	return true;
    	}
	}

	function CheckRevExist()
	{
		$ConfID = $this->session->userdata['LoggedIn']['ConfID']; 
		$Email = $this->input->post('Email');

		$this->db->select('Email');
		$this->db->from('reviewer');
		$this->db->where('ConfID',$ConfID);
		$this->db->where('Email',$Email);


		$query = $this->db->get();

		if ($query->num_rows()>0)
		{
        	return false;
    	}

    	else
    	{
        	return true;
    	}
    }

    function RetPW()
    {
		$ConfID = $this->input->post('ConfID');

		$this->db->select('ChairPW');
		$this->db->from('chairperson');
		$this->db->where('ConfID',$ConfID);

		$query = $this->db->get();
		return $query->row()->ChairPW;
		

		//return $query;    	
    }

    function RetPWRev()
    {
    	$ConfID = $this->session->userdata['LoggedIn']['ConfID'];
    	$Email = $this->input->post('Email');

    	$this->db->select('Password');
		$this->db->from('reviewer');
		$this->db->where('ConfID',$ConfID);
		$this->db->where('Email',$Email);

		$query = $this->db->get();
		return $query->row()->Password;
    }

    function RetConName()
    {
    	$ConfID = $this->session->userdata['LoggedIn']['ConfID'];

    	$this->db->select('ConfLName');
		$this->db->from('conference');
		$this->db->where('ConfID',$ConfID);

		$query = $this->db->get();
		return $query->row()->ConfLName;
    }

    function AddComm()
    {
    	$ConfID = $this->session->userdata['LoggedIn']['ConfID'];

    	$ComName = $this->input->post('ComName');
		$ComHeadName = $this->input->post('ComHeadName');
    	$Email = $this->input->post('Email');
    	$PW = $this->get_random_password();

    	$data = array('ConfID'=>$ConfID, 'ComName'=>$ComName);
    	$this->db->insert('comm',$data);
    	$cid = $this->db->insert_id();

    	$data2 = array('ComID'=>$cid, 'ConfID'=>$ConfID, 'CHName'=>$ComHeadName, 'Email'=>$Email, 'PW'=>$PW);
    	$this->db->insert('commhead',$data2);
    	$cid2 = $this->db->insert_id();

    	$data3 = array('Email'=>$Email,'Password'=>$PW,'ConfID'=>$ConfID,'Type'=>'CH','ID'=>$cid2);
    	$this->db->insert('login',$data3);

    }

    function RetPWCH() //Assumption is that in one conference, one email can only be registered for one committee.
    {
    	$ConfID = $this->session->userdata['LoggedIn']['ConfID'];
    	$Email = $this->input->post('Email');

    	$this->db->select('PW');
		$this->db->from('commhead');
		$this->db->where('ConfID',$ConfID);
		$this->db->where('Email',$Email);

		$query = $this->db->get();
		return $query->row()->PW;
    }

    function AddTracks()
    {
    	$ConfID = $this->session->userdata['LoggedIn']['ConfID'];
    	$TrackName = $this->input->post('Name');

    	$data = array('ConfID'=>$ConfID, 'TrackName'=>$TrackName);
    	$this->db->insert('tracks',$data);

    	return true;
    }

    function ViewCom()
    {
    	$ConfID = $this->session->userdata['LoggedIn']['ConfID'];
      
		$this->db->select('ComName');
		$this->db->from('comm');
		$this->db->where('ConfID',$ConfID);

		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			foreach($query->result() as $row)
			{
				$data[] = $row;
			}

			return $data;
		}

		else
		{
			return false;
		}
    }

    function ViewTracks()
    {
    	$ConfID = $this->session->userdata['LoggedIn']['ConfID'];
      
		$this->db->select('TrackName,TrackID');
		$this->db->from('tracks');
		$this->db->where('ConfID',$ConfID);

		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			foreach($query->result() as $row)
			{
				$data[] = $row;
			}

			return $data;
		}

		else
		{
			return false;
		}
    }

    function CheckTracks()
    {
    	$Name = $this->input->post('Name');
    	$ConfID = $this->session->userdata['LoggedIn']['ConfID'];

    	$this->db->select('TrackID');
		$this->db->from('tracks');
		$this->db->where('ConfID',$ConfID);
		$this->db->where('TrackName',$Name);


		$query = $this->db->get();

		if ($query->num_rows()>0)
		{
        	return false;
    	}

    	else
    	{
        	return true;
    	}
    }

    function DeleteTracks($TrackID)
    {
    	$this->db->where('TrackID', $TrackID);
		$this->db->delete('tracks'); 
    }

     function DeleteRev($RevID) 
    {
    	$ConfID = $this->session->userdata['LoggedIn']['ConfID'];
    	
    	$this->db->where('RID', $RevID);
		$this->db->delete('reviewer');

		$this->db->where('ID', $RevID);
		$this->db->where('ConfID',$ConfID);
		$this->db->where('Type','R');
		$this->db->delete('login');		    	
    }

}
?>