<?php
class AuthorSignupM extends CI_Model
{

	function signup()
	{

		//$this->load->database();
		$ConfName=$this->input->post('ConfName');
		$Name=$this->input->post('name');
		$Email=$this->input->post('email');
		$Password=$this->input->post('pw');
		$Contact=$this->input->post('num');
		$ConfID = $this->AuthorSignupM->RetConfID($ConfName);

		$data=array('Name'=>$Name,'Email'=>$Email,'PW'=>$Password,'Contact'=>$Contact,'ConfID'=>$ConfID);
		$this->db->insert('author',$data);

		$cid = $this->db->insert_id();
		$data2 = array('Email'=>$Email, 'Password'=>$Password, 'ConfID'=>$ConfID, 'Type'=>'A', 'ID'=>$cid);
		$this->db->insert('login',$data2); 
		return true;
	}
	
/*	function uploadPaperToDB()
	{

		$this->load->database();
		$paperPath='';
	   
		$this->db->where('id', $id);
	   
		$data=array('PaperPath'=>$paperPath);
		$this->db->update('author',$data);

		return TRUE;   
	} */

	function ViewAllConf()
	{
		$this->db->select('ConfLName');
		$this->db->from('conference');
		//$query = $this->db->query('SELECT ConfLName FROM conference');
		$query = $this->db->get();
		return $query->result();
		
	}

	function RetConfID($ConfName)
	{
		$this->db->select('ConfID');
		$this->db->from('conference');
		$this->db->where('ConfLName',$ConfName);

		$query = $this->db->get();
		return $query->row()->ConfID;
	}

	function CheckAExist()
	{
		$Email=$this->input->post('email');
		$ConfName=$this->input->post('ConfName');
		$ConfID = $this->AuthorSignupM->RetConfID($ConfName);
				
		$this->db->select('Email');
		$this->db->from('author');
		$this->db->where('ConfID',$ConfID);
		$this->db->where('Email',$Email);

		$query = $this->db->get();

		if ($query->num_rows()>0)
		{
        	return false;
    	}

    	else
    	{
        	return true;
    	}
	}
}

?>