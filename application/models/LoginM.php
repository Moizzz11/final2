<?php
class LoginM extends CI_Model
{
	function login()
	{
	   $Email = $this->input->post('Email');
	   $PW =  $this->input->post('Password');

	   $this -> db -> select('login.Email,login.Password,login.Type,login.ConfID,login.ID,conference.ConfLName');
	   $this -> db -> from('login');
	   $this->  db-> join('conference', 'conference.ConfID = login.ConfID');
	   $this -> db -> where('login.Email', $Email);
	   $this -> db -> where('login.Password',$PW);
	   $this -> db -> limit(1);


	   //$this -> db -> select('login.Email,login.Password,login.Type,login.ConfID,login.ID,conference.ConfLName,comm.ComID');
	   //$this -> db -> from('login');
	   //$this->  db-> join('conference', 'conference.ConfID = login.ConfID');
	   //$this->  db-> join('comm', 'comm.ConfID = login.ConfID');
	   //$this -> db -> where('login.Email', $Email);
	   //$this -> db -> where('login.Password',$PW);
	   //$this -> db -> limit(1);

	   $query = $this -> db -> get();

	   if($query -> num_rows() == 1)
	   {
	     return $query->result();
	   }
	   else
	   {
	     return false;
	   }
	}

	function loginAdmin()
	{
	   $UN = $this->input->post('un');
	   $PW =  $this->input->post('pw');

	   $this -> db -> select('AdminID,AdminUN,AdminPW,AdminName');
	   $this -> db -> from('Admin');
	   $this -> db -> where('AdminUN', $UN);
	   $this -> db -> where('AdminPW', $PW);

	   $query = $this -> db -> get();

	   if($query -> num_rows() == 1)
	   {
 	     return $query->result();
	   }
	   else
	   {
	     return false;
	   }
	}

}

?>