<?php
class ComM extends CI_Model
{

	function AddMem()
	{
		$Name = $this->input->post('MemName');
		$Email = $this->input->post('Email');
		$ConfID = $this->session->userdata['LoggedIn']['ConfID'];
		$this->load->model('AddRevM');
		$PW = $this->AddRevM->get_random_password();
		$CommID = $this->RetCommID();

		$data = array('CommID'=>$CommID,'ConfID'=>$ConfID,'MemName'=>$Name,'MemEmail'=>$Email,'PW'=>$PW);
		$this->db->insert('members',$data);
		$cid = $this->db->insert_id();

		$data2 = array('Email'=>$Email,'Password'=>$PW,'ConfID'=>$ConfID,'Type'=>'CM','ID'=>$cid);
    	$this->db->insert('login',$data2);
	}

	function RetCommID() //Used to retrieve comID when a comhead is adding members. Need this variable when adding rows to member table
	{
		$ComHeadEmail = $this->session->userdata['LoggedIn']['Email'];
		$ConfID = $this->session->userdata['LoggedIn']['ConfID'];

		$this->db->select('ComID');
		$this->db->from('commhead');
		$this->db->where('ConfID',$ConfID);
		$this->db->where('Email',$ComHeadEmail);

		$query = $this->db->get();
		return $query->row()->ComID;

	}



	function CheckMemExist()
	{
		$ConfID = $this->session->userdata['LoggedIn']['ConfID']; 
		$Email = $this->input->post('Email');

		$this->db->select('MemEmail');
		$this->db->from('members');
		$this->db->where('ConfID',$ConfID);
		$this->db->where('MemEmail',$Email);


		$query = $this->db->get();

		if ($query->num_rows()>0)
		{
        	return false;
    	}

    	else
    	{
        	return true;
    	}
	}

	function ViewMem()
	{
		$ConfID = $this->session->userdata['LoggedIn']['ConfID'];
		$CommID = $this->RetCommID();
      
		$this->db->select('MemID,MemName,MemEmail');
		$this->db->from('members');
		$this->db->where('ConfID',$ConfID);
		$this->db->where('CommID',$CommID);

		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			foreach($query->result() as $row)
			{
				$data[] = $row;
			}

			return $data;
		}

		else
		{
			return false;
		}
	}

	function RetPWCM()
    {
    	$ConfID = $this->session->userdata['LoggedIn']['ConfID'];
    	$Email = $this->input->post('Email');

    	$this->db->select('PW');
		$this->db->from('members');
		$this->db->where('ConfID',$ConfID);
		$this->db->where('MemEmail',$Email);

		$query = $this->db->get();
		return $query->row()->PW;
    }

    function RetCommName()
    {
		$CommID = $this->RetCommID();

		$this->db->select('ComName');
		$this->db->from('comm');
		$this->db->where('ComID',$CommID);

		$query = $this->db->get();
		return $query->row()->ComName;    	
    }

    function DeleteMem($MemID)
    {
    	$this->db->where('MemID', $MemID);
		$this->db->delete('members'); 

		$this->db->where('ID', $MemID);
		$this->db->where('Type', 'CM');
		$this->db->delete('login');
    }

    function AddTask1()
    {
    	$ConfID = $this->session->userdata['LoggedIn']['ConfID'];
    	$Task = $this->input->post('1task');
    	$Desc = $this->input->post('desc');
    	$CommID = $this->RetCommID();

    	$data = array('ComID'=>$CommID,'ConfID'=>$ConfID,'TaskName'=>$Task,'Desc'=>$Desc);
		$this->db->insert('task',$data);
    }

    function ViewTasks()
	{
		$ConfID = $this->session->userdata['LoggedIn']['ConfID'];
		$CommID = $this->RetCommID();
      
		$this->db->select('TaskID,ComID,TaskName,Desc');
		$this->db->from('task');
		$this->db->where('ConfID',$ConfID);
		$this->db->where('ComID',$CommID);

		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			foreach($query->result() as $row)
			{
				$data[] = $row;
			}

			return $data;
		}

		else
		{
			return false;
		}
	}

	function DeleteTasks($TaskID)
	{
		$this->db->where('TaskID', $TaskID);
		$this->db->delete('task'); 		
	}

	function CheckCHExist()
	{
		$ConfID = $this->session->userdata['LoggedIn']['ConfID'];
		$Email = $this->input->post('Email');

		$this->db->select('Email');
		$this->db->from('commhead');
		$this->db->where('ConfID',$ConfID);
		$this->db->where('Email',$Email);

		$query = $this->db->get();

		if ($query->num_rows()>0)
		{
        	return false;
    	}

    	else
    	{
        	return true;
    	}
	}

	function RetCommIDMem() //For members
	{
		$ComMemEmail = $this->session->userdata['LoggedIn']['Email'];
		$ConfID = $this->session->userdata['LoggedIn']['ConfID'];
		$MemID = $this->session->userdata['LoggedIn']['ID'];

		$this->db->select('CommID');
		$this->db->from('members');
		$this->db->where('ConfID',$ConfID);
		$this->db->where('MemEmail',$ComMemEmail);
		$this->db->where('MemID',$MemID);

		$query = $this->db->get();
		return $query->row()->CommID;
	}

	function ViewTasksMem()
	{
		$ConfID = $this->session->userdata['LoggedIn']['ConfID'];
		$CommID = $this->RetCommIDMem();
      
		$this->db->select('TaskID,ComID,TaskName,Desc');
		$this->db->from('task');
		$this->db->where('ConfID',$ConfID);
		$this->db->where('ComID',$CommID);

		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			foreach($query->result() as $row)
			{
				$data[] = $row;
			}

			return $data;
		}

		else
		{
			return false;
		}
	}

}

?>