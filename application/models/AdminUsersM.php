<?php
class AdminUsersM extends CI_Model
{
	function deleteChair($ChairID)
	{
		$this->db->delete('chairperson', array('ChairID' => $ChairID)); 

	}

	function deleteAuthors($AuthorID)
	{
		$this->db->delete('author', array('AuthorID' => $AuthorID)); 

	}
	function deleteReview($RID)
	{
		$this->db->delete('reviewer', array('RID' => $RID)); 

	}
	
	function ViewChairUsers()
	{
		$this->db->select('ChairID,ChairName,ConfID,ChairEmail');
		$this->db->from('chairperson');
		
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			foreach($query->result() as $rowc)
			{
				$data[] = $rowc;
			}

			return $data;
		}

		else
		{
			return false;
		}
	}
	
	function ViewReviewUsers()
	{
		$this->db->select('RID,Name,ConfID,Email,Spec');
		$this->db->from('reviewer');
		
				$query = $this->db->get();
		if($query->num_rows()>0)
		{
			foreach($query->result() as $rowr)
			{
				$data[] = $rowr;
			}

			return $data;
		}

		else
		{
			return false;
		}
	}

	function ViewAuthorUsers()
	{
		$this->db->select('AuthorID,Name,ConfID,Email,Contact');
		$this->db->from('author');
		
		$query = $this->db->get();
		if($query->num_rows()>0)
		{
			foreach($query->result() as $rowa)
			{
				$data[] = $rowa;
			}

			return $data;
		}

		else
		{
			return false;
		}
	}
	
}