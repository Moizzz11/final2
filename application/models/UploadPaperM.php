<?php
/*****GET DATA DB*****
		$this->load->database();
		$this -> db -> select('Name');
		$this -> db -> from('author');
		$this -> db -> where('AuthorID', $AuthorID);
		$this -> db -> limit(1);

		$query = $this -> db -> get();
		foreach ($query->result() as $row)
		{
		   return $row->Name;
		}

*****GET DATA SESSION******
$AuthorID = $this->session->userdata('AuthorID');

*/
class UploadPaperM extends CI_Model{
	function getMyOtherPapers($AuthorID){
		$query = $this->db->query("SELECT author.Name,author.Email, file.fileName,file.Abstract,file.FileID FROM author, file, file_owned_by 
									WHERE author.AuthorID=file.FileID AND file.fileID=file_owned_by.fileID
										AND file_owned_by.AuthorID=$AuthorID AND file.UploadedAuthorID!=$AuthorID;");
		return $query;
		
	}
	function getTracks(){
		$tmp = $this->session->userdata('LoggedIn');
		$confID = $tmp['ConfID'];
		$this -> db -> select('TrackID');
		$this -> db -> select('TrackName');
		$this -> db -> from('Tracks');
		$this -> db -> where('ConfID', $confID);
		
		$query = $this -> db -> get();
/*		$i =1;
		$tracks = array();
		foreach ($query->result() as $row)
		{
			//array_push($tracks,'track'=>$row->TrackID);
			$tracks = array($row->TrackName);
			$i++;
		}*/
		return $query;
	}
	
	
	
	
	function getAuthorID($Email, $ConfID){
		$this->load->database();
		$this -> db -> select('AuthorID');
		$this -> db -> from('author');
		$this -> db -> where('ConfID', $ConfID);
		$this -> db -> where('Email', $Email);
		$this -> db -> limit(1);
	
		//echo "</br>email>>>$Email<<<....confID>>>$ConfID<<<</br>";
		$query = $this -> db -> get();
		$res = null;//false;
		
		foreach ($query->result() as $row)
		{
			//echo "aaa>>>$res<<<";
			$res = $row->AuthorID;
		}
		
		return $res;
	}
	function getAbstract($fileID){
		$this->load->database();
		$this -> db -> select('Abstract');
		$this -> db -> from('file');
		$this -> db -> where('FileID', $fileID);
		$this -> db -> limit(1);

		$query = $this -> db -> get();
		foreach ($query->result() as $row)
		{
		   return $row->Abstract;
		}
	}
	
	function getTrack($fileID){
		$this->load->database();
		$this -> db -> select('Track');
		$this -> db -> from('file');
		$this -> db -> where('FileID', $fileID);
		$this -> db -> limit(1);

		$query = $this -> db -> get();
		foreach ($query->result() as $row)
		{
		   return $row->Track;
		}
	}
	function viewForm($a=''){
		
		$AuthorID = $this->session->userdata('AuthorID');
		if($this->isDueDateNotPassed() ){
			$abstract ="";
			$selectedTrack='';
			echo '<div class="alert alert-info">';
			if($a=='init'){
				echo '<h4>Submit Conference Paper</h4></br>';
			}
			else{
				echo '<h4>Edit the submission</h4></br>';
				$abstract = $this->getAbstract($AuthorID);
				$selectedTrack = $this->getTrack($AuthorID);
			}
			
			
			echo form_open_multipart('UploadPaperC/do_upload');
				
			if($a=='init' || $a=='editable' || $a=='file'){
				echo '<div class="alert alert-success">';
				echo '<input type="file" name="userfile" size="20" />';
				echo '</div>';
			}
			if($a=='init' || $a=='editable' || $a=='abstract'){
				echo '<div class="alert alert-success">';
				echo '</br>Abstract</br>
					'."<textarea rows=\"4\"  style=\"width:100%;\" name=\"abstract\" size=\"20\">$abstract</textarea>".'
					</br>';
				echo '</div>';
			}
			if($a=='init' || $a=='editable' || $a=='track'){
				$tracks = $this->getTracks();
	/*	*/		echo '<div class="alert alert-success">';
				echo 'Track : <select name="Track">';
				foreach ($tracks->result() as $row){
					$name = $row->TrackName;
					$value = $row->TrackID;
					$selected='';
					if($value == $selectedTrack){	$selected = 'SELECTED';}
					echo "<option value=\"$value\" $selected>$name</option>";
				}	
				echo '</select>';
				echo '</div>';
			}
			echo '<br /><br />
				<button class="btn btn-large btn-primary" type="submit">Submit</button>';
			echo '</form>';
			echo '</div>';
		}
		else{
			echo '<div class="alert alert-danger">';
			echo 'Due date passed ! , you can\'t upload / edit the paper now';
			echo '</div>';
		}
	}
	function isDueDateNotPassed(){
		$curDate = date('Y-m-d H:i:s');
		$dueDate = $this->getDueDate();
		if($curDate<=$dueDate)
			return true;
		else
			return false;
	}
	function getFileName($AuthorID){
		$this->load->database();
		$this -> db -> select('FileName');
		$this -> db -> from('file');
		$this -> db -> where('FileID', $AuthorID);
		$this -> db -> limit(1);

		$query = $this -> db -> get();
		foreach ($query->result() as $row)
		{
		   return $row->FileName;
		}
	}
	
	function getDueDate(){
		$tmp = $this->session->userdata('LoggedIn');
		$confID = $tmp['ConfID'];
		$this->load->database();
		$this -> db -> select('DueDate');
		$this -> db -> from('conference');
		$this -> db -> where('ConfID', $confID);
		$this -> db -> limit(1);

		$query = $this -> db -> get();
		foreach ($query->result() as $row)
		{
		   return $row->DueDate;
		}
		return null;
	}
	function getAvailableAuthors($fileID){
		$this->load->database();
		$this -> db -> select('AuthorID');
		$this -> db -> from('file_owned_by');
		$this -> db -> where('FileID', $fileID);
		
		$query = $this -> db -> get();
		$ids = array();
		foreach($query->result_array() as $a) {
		   $ids[] = $a['AuthorID'];
		}
		//$ids = $query->result();
		
		$confID = $this->session->userdata('ConfID');
		$this -> db -> select('*');
		$this -> db -> from('author');
		$this -> db -> where_not_in('AuthorID', $ids);
		
		$query = $this -> db -> get();
		return $query;
	}
	function getOwnFileAuthors(){
		$AuthorID = $this->session->userdata('AuthorID');
		
		return $this->getFileAuthors($AuthorID);
	}
	
	function getFileAuthors($fileID){
		$this->load->database();
		$this -> db -> select('AuthorID');
		//$this -> db -> select('author.Email');
		//$this -> db -> from('author');
		$this -> db -> from('file_owned_by');
		//$this -> db -> join('file_owned_by', 'file_owned_by.FileID = author.AuthorID');
		$this -> db -> where('file_owned_by.FileID', $fileID);
		//$this -> db -> where('author.AuthorID', $fileID);
		//$this -> db -> where('file_owned_by.FileID=author.AuthorID');
		//$where ="authorID=1";
		//$this -> db -> where($where);
		
		$query = $this -> db -> get();
		return $query;
	}
	
	function getAuthorName($AuthorID){
		$this->load->database();
		$this -> db -> select('Name');
		$this -> db -> from('author');
		$this -> db -> where('AuthorID', $AuthorID);
		$this -> db -> limit(1);

		$query = $this -> db -> get();
		foreach ($query->result() as $row)
		{
		   return $row->Name;
		}
	}
	function getAuthorEmail($AuthorID){
		$this->load->database();
		$this -> db -> select('Email');
		$this -> db -> from('author');
		$this -> db -> where('AuthorID', $AuthorID);
		$this -> db -> limit(1);

		$query = $this -> db -> get();
		foreach ($query->result() as $row)
		{
		   return $row->Email;
		}
	}
	function getFileID($AuthorID){
		$this->load->database();
		$this -> db -> select('FileID');
		$this -> db -> from('file_owned_by');
		$this -> db -> where('AuthorID', $AuthorID);
		$this -> db -> limit(1);

		$query = $this -> db -> get();
		foreach ($query->result() as $row)
		{
		   return $row->FileID;
		}
	}
	
	function canAuthorUploadAPaper($authorID)
	{
		//$this->load->database();
		//return 'noFileDueDatePassed';
		$this->db->select('FileID');
		$this->db->from("file");
		$this->db->where('UploadedAuthorID',$authorID);
		$count = -3;
		$query = $this->db->get();
		
		$count = $query->num_rows(); 
		if(  $this->isDueDateNotPassed()==true){
			if( $count==0 ){
				return 'init';
			}
			return 'editable';
		}
		else{
			if( $count==0 ){
				return 'noFileDueDatePassed';
			}
			return 'noneditable';
		}
	
	}
	function uploadPaperToDB($data, $Track, $Abstract)
	{
			//session_start();
			$UploadedAuthorID = $this->session->userdata('AuthorID');
			$ConfID = $this->session->userdata['LoggedIn']['ConfID'];
			date_default_timezone_set('Asia/Colombo');	//fix this to get the correct time
			$date = date('Y-m-d H:i:s');
			$LastUpdatedTime=$date;
			$this->load->database();
			$i=0;
			$FileName;
			if($data!=null){
				$upload_data = $data['upload_data'];
				foreach ($upload_data as $item => $value){
				 if($i==0)
				 {
				   $FileName = $value;
				 }
				 $i++;
				}
			}
			$res = $this->canAuthorUploadAPaper($UploadedAuthorID);
			$uploadFolderPath ='../uploads/';
			if($res =='init'){
				if(!isset($upload_data) ){
					return 'noFile';
				}
				/*
				$this->db->select("COUNT(*) AS MyCount");
				$this->db->from("file");
				$count = '-3';
				$count = $this->db->count_all_results() +1; 
				*/
				
				$FileID = $UploadedAuthorID;
				
				/*  FileID int(10) PRIMARY KEY,
					FileName varchar(100) NOT NULL,
					LastUpdatedTime DATETIME,
					UploadedAuthorID int(10),
					ReviewStatus VARCHAR(20) DEFAULT 'NotReviewed'
				*/
				
				$data=array('FileID'=>$FileID, 'FileName'=>$uploadFolderPath.$FileName,
												'LastUpdatedTime'=>$LastUpdatedTime ,
												'UploadedAuthorID'=>$UploadedAuthorID
												,'Track'=>$Track
												,'Abstract'=>$Abstract, 'ConfID'=>$ConfID);
				$this->db->insert('file',$data);
				$data1= array('FileID'=>$FileID ,'AuthorID'=>$UploadedAuthorID);
				$this->db->insert('file_owned_by',$data1);
				return $UploadedAuthorID;  
			}
			else if($res =='editable' && isset($upload_data)){
				$this->db->where('UploadedAuthorID' , $UploadedAuthorID);
				$data = array('FileName'=>$uploadFolderPath.$FileName,'LastUpdatedTime'=>$LastUpdatedTime,'Track'=>$Track, 'Abstract'=>$Abstract);
				$this->db->update('file' , $data);
			}
			else if($res =='editable' && !isset($upload_data)){
				$this->db->where('UploadedAuthorID' , $UploadedAuthorID);
				$data = array('LastUpdatedTime'=>$LastUpdatedTime,'Track'=>$Track, 'Abstract'=>$Abstract);
				$this->db->update('file' , $data);
			}
			 
		//}
		return 'oooooooooook';
	}
	
	
}
?>