<?php include_once('header.php'); ?>

<body>
<div class="container">
	<div class="page-header">
		<center>
			<h1>SLIIT Conference Management System</h1>
			<small>Committee Head Dasboard</small>
		</center>
		
		<!-- Navbar Start!-->
		<nav class="navbar navbar-inverse" role="navigation">
		<div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="ComHDashC">Committee Head Home</a>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li class="dropdown">
          		<a href="#" class="dropdown-toggle" data-toggle="dropdown">Members<b class="caret"></b></a>
			         <ul class="dropdown-menu">
			            <li><a href="AddMemC">Add Members</a>
			            <li><a href="DelMemC">Delete Members</a>
		          	</ul>
        		</li>

        		<li class="dropdown">
          		<a href="#" class="dropdown-toggle" data-toggle="dropdown">Tasks<b class="caret"></b></a>
			         <ul class="dropdown-menu">
			            <li><a href="AddTasksC">Add Tasks</a>
			          
		          	</ul>
        		</li>
						
			</ul>
		
		  <ul class="nav navbar-nav navbar-right">
			<li class="dropdown">
			  <a href="#" class="dropdown-toggle" data-toggle="dropdown">More Options <b class="caret"></b></a>
			  <ul class="dropdown-menu">
				<li><a href="LoginC/logout">Logout</a></li>
				
			  </ul>
			</li>
		  </ul>
		</div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
		<!-- Navbar End!-->
		
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Add Members to your committee:</h3>
			</div>
				<div class="panel-body">

					<?php if(validation_errors()):?>
			         	<div class="alert alert-danger">
			               	<?php echo validation_errors(); ?>
			            </div>
			        <?php endif;?>
					
						<form role="form" action="" method="post" id="ConRequest"> 
						<div class="form-group">
							<label for="CommName">Add Member Name</label>
							<input type="text" class="form-control" id="MemName" name="MemName" placeholder="Enter Name">
						</div>
						<div class="form-group">
							<label for="CommHeadName">Add Member Email</label>
							<input type="text" class="form-control" id="Email" name="Email" placeholder="Enter Email">
						</div>
						<input type="submit" class="btn btn-large btn-primary" id="submit" name="submit" value="Add"> 
					</form>

				</div>
		</div>


		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Existing Members:</h3>
			</div>
				<div class="panel-body">
					<?php if ($row){ ?>
						<table class="table table-striped">
							            <thead>
							 
							                    <tr>
							                                <th>Member Name</th>
							                                <th>Member Email</th>
							                    </tr>
							            </thead>

							         	<?php foreach($row as $result)
							              { ?>
							              	<tr>
							                                    <td><?php echo $result->MemName;?></td>
							                                    <td><?php echo $result->MemEmail;?></td>
							                                                                      
							                </tr>
							            <?php }?>
					<?php }?>
				            
				        </table>

				        <?php 
                                             if (!$row) {
                                        echo 'No data to display';
                                         } ?>

        	</div>
		</div>
	
</div> <!-- end of main container -->
<script>
	$('.dropdown-toggle').dropdown()
</script>
<?php include_once('footer.php'); ?>