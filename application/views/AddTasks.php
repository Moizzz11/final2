<?php include_once('header.php'); ?>

<body>
<div class="container">
	<div class="page-header">
		<center>
			<h1>SLIIT Conference Management System</h1>
			<small>Committee Head Dasboard</small>
		</center>
		
		<!-- Navbar Start!-->
		<nav class="navbar navbar-inverse" role="navigation">
		<div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="ComHDashC">Committee Head Home</a>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li class="dropdown">
          		<a href="#" class="dropdown-toggle" data-toggle="dropdown">Members<b class="caret"></b></a>
			        <ul class="dropdown-menu">
			            <li><a href="AddMemC">Add Members</a>
			            <li><a href="DelMemC">Delete Members</a>
		          	</ul>
        		</li>

        		<li class="dropdown">
          		<a href="#" class="dropdown-toggle" data-toggle="dropdown">Tasks<b class="caret"></b></a>
			        <ul class="dropdown-menu">
			            <li><a href="AddTasksC">Add Tasks</a>
			          
		          	</ul>
        		</li>	
			</ul>
		
		  <ul class="nav navbar-nav navbar-right">
			<li class="dropdown">
			  <a href="#" class="dropdown-toggle" data-toggle="dropdown">More Options <b class="caret"></b></a>
			  <ul class="dropdown-menu">
				<li><a href="LoginC/logout">Logout</a></li>
				
			  </ul>
			</li>
		  </ul>
		</div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
		<!-- Navbar End!-->
		
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Added Tasks:</h3>
			</div>
				<div class="panel-body">
					<?php if(validation_errors()):?>
			         	<div class="alert alert-danger">
			               	<?php echo validation_errors(); ?>
			            </div>
			        <?php endif;?>	
					<table class="table table-striped">
							            <thead>
							 
							                    <tr>
							                                <th>Task Name</th>
							                                <th>Task Description</th>
							                    </tr>
							            </thead>

							         	<?php foreach($row as $result)
							              { ?>
							              <?php $TaskID = $result->TaskID; ?>
							              <?php $Desc = $result->Desc; ?>
							              	<tr>
							                                    <td><?php echo $result->TaskName;?></td>
							                                    <td><?php echo $result->Desc;?></td>
							                                    <?php echo "<td>". anchor('AddTasksC/deleteValue/'.$TaskID,'Delete') ."</td>";     
                      echo "</tr>";?>
							                                                                      
							                </tr>
							            <?php }?>
				            
				        </table>

					
					<br><br>
				<input type="submit" class="btn btn-large btn-success" data-toggle="modal" data-target="#myModal" id="task" name="task" value="Add New Tasks"> 
				</div>
		</div>

		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Add New Tasks</h4>
      </div>
      <div class="modal-body">
        			<?php if(validation_errors()):?>
			         	<div class="alert alert-danger">
			               	<?php echo validation_errors(); ?>
			            </div>
			        <?php endif;?>
					
						<form role="form" action="" method="post" id="ConRequest"> 
							<div class="form-group">
								<label for="CommName">Add Task Name</label>
								<input type="text" class="form-control" id="1task" name="1task" placeholder="Enter Task">
							</div>

							<div class="form-group">
								<label for="CommName">Add Task Description</label>
								<input type="text" class="form-control" id="desc" name="desc" placeholder="Enter Task Description">
							</div>
							<input type="submit" class="btn btn-large btn-primary" id="task1" name="task1" value="Add"> 
						</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
       <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>
</div>
	
</div> <!-- end of main container -->
<script>
	$('.dropdown-toggle').dropdown()
</script>
<?php include_once('footer.php'); ?>