<?php include_once('header.php'); ?>
<body>
<div class="container">
	<div class="page-header">
		<center>
			<h1>SLIIT Conference Management System</h1>
			<small>Administrator Dasboard</small>
		</center>
		
		<?php if(validation_errors()):?>
         	<div class="alert alert-danger">
               	<?php echo validation_errors(); ?>
            </div>
        <?php endif;?>
        

		<form role="form" action="" method="post">
		<div class="form-group">
			<label for="ConfID">Enter Conference ID?</label>
			<input type="text" class="form-control" id="ConfID" name="ConfID" placeholder="Enter Conference ID">
		</div>
		
		<div class="form-group">
			<label for="ConfChairName">Enter Chair Name?</label>
			<input type="text" class="form-control" id="ConfChairName" name="ConfChairName" placeholder="Enter Chair Name">
		</div>
		
		<div class="form-group">
			<label for="ConfChairEmail">Enter Email?</label>
			<input type="text" class="form-control" id="ConfChairEmail" name="ConfChairEmail" placeholder="Enter Email">
		</div>
		
		<input type="submit" class="btn btn-large btn-primary" id="submit" name="submit" value="Submit">  &nbsp;
		<a href="AdminConReqC">Back</button>
	</form>
			
</div> <!-- end of main container -->
<?php include_once('footer.php'); ?>
