<?php include_once('header.php'); ?>
<body>
<div class="container">
 <div class="page-header">
   <center>
     <h1>SLIIT Conference Management System</h1>
     <small>Author Dashboard</small>
   </center>
   
   <!-- Navbar Start!-->
   <nav class="navbar navbar-inverse" role="navigation">
   <div class="container-fluid">
   <!-- Brand and toggle get grouped for better mobile display -->
   <div class="navbar-header">
     <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
       <span class="sr-only">Toggle navigation</span>
       <span class="icon-bar"></span>
       <span class="icon-bar"></span>
       <span class="icon-bar"></span>
     </button>
     
   </div>

   <!-- Collect the nav links, forms, and other content for toggling -->
   <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
     <ul class="nav navbar-nav">
       <li class="active"><a href="AuthorHomeC">Author Home</a></li>
       <li><a href="EditAuthorProfileC">Edit Profile</a></li>
       <li><a href="UploadPaperC">Submit Paper</a></li>
       <li><a href="CheckReviewStatusC">Check Review Status</a></li>      
     </ul>
   
     <ul class="nav navbar-nav navbar-right">
       <li class="dropdown">
         <a href="#" class="dropdown-toggle" data-toggle="dropdown">More Options <b class="caret"></b></a>
         <ul class="dropdown-menu">
           <li><a href="LoginC/logout">Logout</a></li>
           
         </ul>
       </li>
     </ul>
   </div><!-- /.navbar-collapse -->
 </div><!-- /.container-fluid -->
</nav>
   <!-- Navbar End!-->
   
   <form role="form" action="AuthorHome.php" method="post">
       
 </form>
 <br>
 
</div> <!-- end of main container -->
<?php include_once('footer.php'); ?>