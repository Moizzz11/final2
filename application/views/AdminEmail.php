<?php include_once('header.php'); ?>
<body>
<div class="container">
	<div class="page-header">
		<center>
			<h1>SLIIT Conference Management System</h1>
			<small>Administrator Dasboard</small>
		</center>
		
		<!-- Navbar Start!-->
		<nav class="navbar navbar-inverse" role="navigation">
		<div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="AdminDashC">Admin Home</a>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li><a href="AdminConReqC">Create Conference</a></li>
				<li><a href="AdminAllConC">All Conferences</a></li>
				<li><a href="AdminAllUsersC">ChairPersons</a></li>
				<li><a href="AdminUsersC">All Users</a></li>
				<li class="active"><a href="AdminEmailC">Email Details</a></li>			
			</ul>
		
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">More Options <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="LoginC/logoutAdmin">Logout</a></li>
            
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
		<!-- Navbar End!-->
		
		<form role="form" action="" method="post">
			<div class="form-group">
				<label for="ChairEmail">Enter Email Address</label>
				<input type="text" class="form-control" id="ChairEmail" name="ChairEmail" placeholder="Email">
			</div>
			<div class="form-group">
				<label for="Subject">Enter Subject</label>
				<input type="text" class="form-control" id="Subject" name="Subject" placeholder="Subject" value="Your conference request has been rejected">
			</div>
			<div class="form-group">
				<label for="Body">Email Body</label>
				<textarea style="width:1137px;"class="field span48" id="Body" name="Body" rows="10" placeholder="Enter Email Body">Thank you for your interest in creating a conference at SLIIT Conference Management Toolkit. However, we were unable to process your request. We suggest that you try re-sending a request with a more verified data. Thank you!</textarea>
			</div>
			
			<input type="submit" class="btn btn-large btn-primary" id="submit" name="submit" value="Send">
		
		</form>
	<br>
		
	
</div> <!-- end of main container -->
<?php include_once('footer.php'); ?>