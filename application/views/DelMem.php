<?php include_once('header.php'); ?>

<body>
<div class="container">
	<div class="page-header">
		<center>
			<h1>SLIIT Conference Management System</h1>
			<small>Committee Head Dasboard</small>
		</center>
		
		<!-- Navbar Start!-->
		<nav class="navbar navbar-inverse" role="navigation">
		<div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="ComHDashC">Committee Head Home</a>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li class="dropdown">
          		<a href="#" class="dropdown-toggle" data-toggle="dropdown">Members<b class="caret"></b></a>
			         <ul class="dropdown-menu">
			            <li><a href="AddMemC">Add Members</a>
			            <li><a href="DelMemC">Delete Members</a>
		          	</ul>
        		</li>

        		<li class="dropdown">
          		<a href="#" class="dropdown-toggle" data-toggle="dropdown">Tasks<b class="caret"></b></a>
			         <ul class="dropdown-menu">
			            <li><a href="AddTasksC">Add Tasks</a>
			          
		          	</ul>
        		</li>
						
			</ul>
		
		  <ul class="nav navbar-nav navbar-right">
			<li class="dropdown">
			  <a href="#" class="dropdown-toggle" data-toggle="dropdown">More Options <b class="caret"></b></a>
			  <ul class="dropdown-menu">
				<li><a href="LoginC/logout">Logout</a></li>
				
			  </ul>
			</li>
		  </ul>
		</div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
		<!-- Navbar End!-->
		
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Delete Committee Members</h3>
			</div>
				<div class="panel-body">
					<?php if ($row){ ?>
					<?php
						echo "<div class=\"table-responsive\">
						<table class=\"table table-striped\">
						                           <thread>      
						                                   <tr>
						                                     <th >Member Name</th>
						                                       <th >Member Email</th>						                                  
						  									</tr>
						  							</thread>";
						foreach ($row as $result) {
						$Name = $result->MemName;
						$Email = $result->MemEmail;
						$MemID = $result->MemID;

						echo "<td >" . $Name . "</td>";
						echo "<td >" . $Email . "</td>";
						
						echo "<td>". anchor('DelMemC/deleteValue/'.$MemID,'Delete') ."</td>";     
						                      echo "</tr>";
						}
					
						                           echo "</table></div>";
					?>
					<?php }?>
					<?php 
                                             if (!$row) {
                                        echo 'No data to display';
                                         } ?>
				</div>
		</div>
	
</div> <!-- end of main container -->
<script>
	$('.dropdown-toggle').dropdown()
</script>
<?php include_once('footer.php'); ?>