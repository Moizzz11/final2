<?php include_once('header.php'); ?>

<body>
<div class="container">
	<div class="page-header">
		<center>
			<h1>SLIIT Conference Management System</h1>
			<small>Administrator Login</small>
		</center>
	</div>

	<?php if(validation_errors()):?>
         	<div class="alert alert-danger">
               	<?php echo validation_errors(); ?>
            </div>
    <?php endif;?>

		<div class="container">
			<form class="form-signin" method="post" action="">
				<h2 class="form-signin-heading">Please sign in</h2>
				<input type="text" id="un" name="un"class="input-block-level" placeholder="Username">
				<input type="password" id="pw" name="pw" class="input-block-level" placeholder="Password">
			<button class="btn btn-large btn-primary" type="submit">Sign in</button>
			</form>
		</div>	
</div> <!-- end of main container -->


<?php include_once('footer.php'); ?>

