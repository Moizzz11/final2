<?php include_once('header.php'); ?>
<body>
<div class="container">
<div class="page-header">
<center>
<h1>SLIIT Conference Management System</h1>
<small>Administrator Dasboard</small>
</center>

<!-- Navbar Start!-->
<nav class="navbar navbar-inverse" role="navigation">
<div class="container-fluid">
<!-- Brand and toggle get grouped for better mobile display -->
<div class="navbar-header">
<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a class="navbar-brand" href="AdminDashC">Admin Home</a>
</div>
<!-- Collect the nav links, forms, and other content for toggling -->
<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
<ul class="nav navbar-nav">
<li><a href="AdminConReqC">Create Conference</a></li>
<li class="active"><a href="AdminAllConC">All Conferences</a></li>
<li><a href="AdminAllUsersC">ChairPersons</a></li>
<li><a href="AdminUsersC">All Users</a></li>
<li><a href="AdminEmailC">Email Details</a></li>	
</ul>

     <ul class="nav navbar-nav navbar-right">
       <li class="dropdown">
         <a href="#" class="dropdown-toggle" data-toggle="dropdown">More Options <b class="caret"></b></a>
         <ul class="dropdown-menu">
           <li><a href="LoginC/logoutAdmin">Logout</a></li>
           
         </ul>
       </li>
     </ul>
   </div><!-- /.navbar-collapse -->
 </div><!-- /.container-fluid -->
</nav>
<!-- Navbar End!-->
<div class="panel panel-default">
<div class="panel-heading">
<h3 class="panel-title">All Conferences:</h3>
</div>

<div class="panel-body">
<?php
if ($row){ 
echo "<div class=\"table-responsive\">
<table class=\"table table-striped\">
                           <thread>      
                                   <tr>
                                     <th >ConferenceID</th>
                                       <th >ConferenceName</th>
                                        <th>ConfShort</th>
<th>Due Date</th>
<th>Conference Sub</th>
<th>Delete</th>
                                  
  </tr>
  </thread>";
foreach ($row as $result) {
$ConfID = $result->ConfID;
$ConfLname = $result->ConfLName;
$ConfSName = $result->ConfSName;
$DueDate = $result->DueDate;
$ConfSub = $result->ConfSub;

//echo $result->Email;
echo "<td >" . $ConfID . "</td>";
echo "<td >" . $ConfLname . "</td>";
echo "<td >" . $ConfSName . "</td>";

echo "<td >" . $DueDate . "</td>";
echo "<td>" . $ConfSub . "</td>";
/*echo " <td>
<form action= AdminAllConfC  method= 'post'>
<input type= 'submit' class='btn btn-large btn-primary' name= 'type' value= 'Delete'/>	
</form></td>"; */
/*echo "<td>". anchor('AdminAllConfC/deleteValue'.$result->ConfID,'Delete') ."</td>";*/
echo "<td>". anchor('AdminAllConC/deleteValue/'.$ConfID,'Delete') ."</td>";     
                      echo "</tr>";
}
                                    
                           echo "</table></div>";
}
?>
 <?php 
                                             if (!$row) {
                                        echo 'No data to display';
                                         } ?>
</div>
</div> <!-- end of main container -->
<?php include_once('footer.php'); ?>