<?php include_once('header.php'); ?>
<body>
<div class="container">
<div class="page-header">
<center>
<h1>SLIIT Conference Management System</h1>
<small>Administrator Dasboard</small>
</center>

<!-- Navbar Start!-->
<nav class="navbar navbar-inverse" role="navigation">
<div class="container-fluid">
<!-- Brand and toggle get grouped for better mobile display -->
<div class="navbar-header">
<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a class="navbar-brand" href="AdminDashC">Admin Home</a>
</div>
<!-- Collect the nav links, forms, and other content for toggling -->
<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
<ul class="nav navbar-nav">
<li><a href="AdminConReqC">Create Conference</a></li>
<li ><a href="AdminAllConC">All Conferences</a></li>
<li><a href="AdminAllUsersC">ChairPersons</a></li>
<li class="active"><a href="AdminUsersC">All Users</a></li>
<li><a href="AdminEmailC">Email Details</a></li>	
</ul>

     <ul class="nav navbar-nav navbar-right">
       <li class="dropdown">
         <a href="#" class="dropdown-toggle" data-toggle="dropdown">More Options <b class="caret"></b></a>
         <ul class="dropdown-menu">
           <li><a href="LoginC/logoutAdmin">Logout</a></li>
           
         </ul>
       </li>
     </ul>
   </div><!-- /.navbar-collapse -->
 </div><!-- /.container-fluid -->
</nav>
<!-- Navbar End!-->
<div class="panel panel-default">
<div class="panel-heading">
<h3 class="panel-title">All ChairPersons:</h3>
</div>

<div class="panel-body">
<?php
if ($rowc){ 
echo "<div class=\"table-responsive\">
<table class=\"table table-striped\">
                           <thread>      
                                   <tr>
                                     <th >ChairPersonID</th>
                                       <th >Name</th>
                                        <th>ConferenceID</th>
<th>Email Address</th>
<th>Delete</th>
                                  
  </tr>
  </thread>";
foreach ($rowc as $result) {
$ChairID = $result->ChairID;
$ChairName = $result->ChairName;
$ConfID = $result->ConfID;
$ChairEmail = $result->ChairEmail;

//echo $result->Email;
echo "<td >" . $ChairID . "</td>";
echo "<td >" . $ChairName . "</td>";
echo "<td >" . $ConfID . "</td>";
echo "<td >" . $ChairEmail . "</td>";
echo "<td>". anchor('AdminUsersC/deleteChair/'.$ChairID,'Delete') ."</td>";     
                      echo "</tr>";
}
                                    
                           echo "</table></div>";
}
?>
 <?php 
                                             if (!$rowc) {
                                        echo 'No data to display';
                                         } ?>

</div>
</div> <!-- end of main container -->

<!-- reviewers -->

<div class="panel panel-default">
<div class="panel-heading">
<h3 class="panel-title">All reviewers:</h3>
</div>

<div class="panel-body">

<?php
if ($rowr){ 
echo "<div class=\"table-responsive\">
<table class=\"table table-striped\">
                           <thread>      
                                   <tr>
                                     <th >ReviewerID</th>
                                       <th >Name</th>
                                        <th>ConferenceID</th>
<th>Email Address</th>
<th>Specialization</th>
<th>Delete</th>
                                  
  </tr>
  </thread>";
foreach ($rowr as $result) {


$RID = $result->RID;
$Name = $result->Name;
$ConfID = $result->ConfID;
$Email = $result->Email;
$Spec = $result->Spec;

echo "<td >" . $RID . "</td>";
echo "<td >" . $Name . "</td>";
echo "<td >" . $ConfID . "</td>";
echo "<td >" . $Email . "</td>";
echo "<td >" . $Spec . "</td>";
echo "<td>". anchor('AdminUsersC/deleteReview/'.$RID,'Delete') ."</td>";     
                      echo "</tr>";
}
                                    
                           echo "</table></div>";
        }
?>
 <?php 
                                             if (!$rowr) {
                                        echo 'No data to display';
                                         } ?>
</div>
</div> <!-- end of main container -->

<!-- authors -->

<div class="panel panel-default">
<div class="panel-heading">
<h3 class="panel-title">All Authors:</h3>
</div>

<div class="panel-body">

<?php
if ($rowa){ 
echo "<div class=\"table-responsive\">
<table class=\"table table-striped\">
                           <thread>      
                                   <tr>
                                     <th >AuthorID</th>
                                       <th >Name</th>
                                        <th>ConferenceID</th>
<th>Email Address</th>
<th>Contact</th>
<th>Delete</th>
                                  
  </tr>
  </thread>";

foreach ($rowa as $result) {
$AuthorID = $result->AuthorID;
$Name = $result->Name;
$ConfID = $result->ConfID;
$Email = $result->Email;
$Contact = $result->Contact;

echo "<td >" . $AuthorID . "</td>";
echo "<td >" . $Name . "</td>";
echo "<td >" . $ConfID . "</td>";
echo "<td >" . $Email . "</td>";
echo "<td >" . $Contact . "</td>";
echo "<td>". anchor('AdminUsersC/deleteAuthors/'.$AuthorID,'Delete') ."</td>";     
                      echo "</tr>";
}
                                    
                           echo "</table></div>";
}
?>
 <?php 
                                             if (!$rowa) {
                                        echo 'No data to display';
                                         } ?>
</div>
</div> <!-- end of main container -->



<?php include_once('footer.php'); ?>