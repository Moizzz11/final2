<?php include_once('header.php'); ?>
<body>
<?php include_once('loggedInPanel.php'); ?>

<div class="container">
 <div class="page-header">
   <center>
     <h1>SLIIT Conference Management System</h1>
     <small>Author Dashboard</small>
   </center>
   
   <!-- Navbar Start!-->
   <nav class="navbar navbar-inverse" role="navigation">
   <div class="container-fluid">
   <!-- Brand and toggle get grouped for better mobile display -->
   <div class="navbar-header">
     <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
       <span class="sr-only">Toggle navigation</span>
       <span class="icon-bar"></span>
       <span class="icon-bar"></span>
       <span class="icon-bar"></span>
     </button>
   </div>

   <!-- Collect the nav links, forms, and other content for toggling -->
   <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
     <ul class="nav navbar-nav">
       <li><a href="AuthorHomeC">Author Home</a></li>
       
       <li  class="active"><a href="UploadPaperC">Submit Paper</a></li> 
           </ul>
    </ul>
   
     <ul class="nav navbar-nav navbar-right">
       <li class="dropdown">
         <a href="#" class="dropdown-toggle" data-toggle="dropdown">More Options <b class="caret"></b></a>
         <ul class="dropdown-menu">
           <li><a href="LoginC/logout">Logout</a></li>
           
         </ul>
       </li>
     </ul>
   </div><!-- /.navbar-collapse -->
 </div><!-- /.container-fluid -->
</nav>
   <!-- Navbar End!-->
   
<?php 
$this->load->model('UploadPaperM');
//$dueDate = $this->UploadPaperM->getDueDate();
$AuthorID = $this->session->userdata('AuthorID');

//echo "<h4>Due Date : $dueDate</h4> </br>";

if(isset($error)) { ?>
<div class="alert alert-danger">
    <strong>Error!</strong> <?php echo $error; ?>
</div>
<?php  }
	else if(isset($upload) )
	{	
		$isInitUpload = $this->UploadPaperM->canAuthorUploadAPaper($AuthorID);
		if($isInitUpload){
			$this->UploadPaperM->viewForm('init');
		}
	}
	

	else if(isset($viewFile)) {
	
	$fileName = $this->UploadPaperM->getFileName($AuthorID);
			
		if($viewFile == 'editable'){
			$myFileAuthors = $this->UploadPaperM->getOwnFileAuthors();
			$noOfRows = $myFileAuthors->num_rows();
			$count =0;
			echo "<a href=\"$fileName\"><img src=\"../images/icon.png\" width=\"50px\" height =\"50px\">Download File</img></a></BR></BR>";

			echo '<div class="alert alert-success">';
			if($noOfRows >0){
				echo '<ul>';
				echo '<strong>Add Other Authors of the Paper <br> Note that these authors must be registered for the same conference.</strong></br>';
				foreach ($myFileAuthors->result() as $row){
						$id =$row->AuthorID;
						$email =$this->UploadPaperM->getAuthorEmail($id);
						
						$Name = $this->UploadPaperM->getAuthorName($id);
						//echo $successMessage;
						echo "<li> Name:$Name</br>Email:$email</br></li>";
						$count ++;
					}
				echo '</ul></br>';
				
					
			}
			if($count<3){
				echo form_open_multipart('UploadPaperC/add_author');
				
				?>
				<input type="text" name="authorSelect"/>
				 
				<button class="btn btn-large btn-primary" type="submit">Add</button>
				</form>
				<?php
			}
			echo '</div>';
			$this->UploadPaperM->viewForm('editable');
			
			
		}
		else if($viewFile =='noneditable'){
			echo "<a href=\"$fileName\"><img src=\"../images/icon.png\" width=\"50px\" height =\"50px\">Download File</img></a>";
		}
		else if($viewFile =='noFileDueDatePassed'){
			$dueDate = $this->UploadPaperM->getDueDate(); ?>
			<div class="alert alert-danger">
			<a href="AuthorHomeC" class="close" data-dismiss="danger">&times;</a>
			<strong>Warning!</strong> <?php echo "Due Date ($dueDate) has passed. Your paper cannot be uploaded."; ?>
			</div>
			<?php
		}
		else{
			$myFileAuthors = $this->UploadPaperM->getOwnFileAuthors();
			$noOfRows = $myFileAuthors->num_rows();
			if($noOfRows >0){
				echo '<ul>';
					foreach ($myFileAuthors->result() as $row){
						$id =$row->AuthorID;
						$Name = $this->UploadPaperM->getAuthorName($id);
						echo "<li>ID:$id Name:$Name </li>";
					}
				echo '</ul>';
			}
		}
	} 
	else if(isset($upload_data))
	{ 
	echo '<div class="alert alert-success">';
	echo '<a href="../UploadPaperC" class="close" data-dismiss="success">&times;</a>';
	echo '<h4> Your paper has been submitted for reviewing.<br> The review status will be notified via Email.</h4>';;
	echo '</br><strong>File Successfully Uploaded.!</strong></br>';
	//echo '<ul>';
	$i=0;
	foreach ($upload_data as $item => $value):
		
    
		if($i==0 || $i==1 || $i==8)
		{
			echo'<b>';
			echo $item;
			echo ':</b> ';
			echo $value;
			echo '</br>';
		}
	 $i++;
	endforeach;
	echo '</div>';
	//echo '</ul>';
	}
	else if(isset($successMessage)){
		echo '<div class="alert alert-success">';
		echo '<a href="../AuthorHomeC" class="close" data-dismiss="success">&times;</a>';
	echo '<h4> Your paper has been submitted for reviewing.<br> The review status will be notified via Email</h4>';;
		echo '<strong></strong></br>';
		echo $successMessage;
		echo '</div>';
	}
	
	
?>
<?php include_once('footer.php'); ?>