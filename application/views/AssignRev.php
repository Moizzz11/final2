<?php include_once('header.php'); ?>
<body>
<div class="container">
	<div class="page-header">
		<center>
			<h1>SLIIT Conference Management System</h1>
			<small>Chairperson Dasboard</small>
		</center>
		
		<!-- Navbar Start!-->
		<nav class="navbar navbar-inverse" role="navigation">
		<div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="ChairDashC">Chair Home</a>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li class="dropdown">
          		<a href="AddRevC" class="dropdown-toggle" data-toggle="dropdown">Reviewers <b class="caret"></b></a>
			         <ul class="dropdown-menu">
			            <li><a href="AddRevC">Add </a></li>
			            <li><a href="ViewRevC">View all Reviewers</a></li>
		          	</ul>
        		</li>		
			</ul>

			<ul class="nav navbar-nav">
				<li class="dropdown">
          		<a href="AddRevC" class="dropdown-toggle" data-toggle="dropdown">Committees <b class="caret"></b></a>
			         <ul class="dropdown-menu">
			            <li><a href="AddComC">Add</a></li>
			            <li><a href="ViewComC">View all Committees</a></li>
		          	</ul>
        		</li>			
			</ul>

			<ul class="nav navbar-nav">
				<li class="dropdown">
          		<a href="AddTracksC" class="dropdown-toggle" data-toggle="dropdown">Tracks <b class="caret"></b></a>
			         <ul class="dropdown-menu">
			            <li><a href="AddTracksC">Add New</a></li>
		          	</ul>
        		</li>			
			</ul>


			<ul class="nav navbar-nav">
				<li class="dropdown">
          		<a href="AssignRevC" class="dropdown-toggle" data-toggle="dropdown">Papers <b class="caret"></b></a>
			         <ul class="dropdown-menu">
			            <li><a href="AssignRevC">Assign Papers</a></li>
			            <li><a href="ViewPapersC">View Papers</a></li>
		          	</ul>
        		</li>			
			</ul>
		
		  <ul class="nav navbar-nav navbar-right">
			<li class="dropdown">
			  <a href="#" class="dropdown-toggle" data-toggle="dropdown">More Options <b class="caret"></b></a>
			  <ul class="dropdown-menu">
				<li><a href="LoginC/logout">Logout</a></li>
				
			  </ul>
			</li>
		  </ul>
		</div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
		<!-- Navbar End!-->
		
		<?php if(validation_errors()):?>
         	<div class="alert alert-danger">
               	<?php echo validation_errors(); ?>
            </div>
        <?php endif;?>



		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Assign Reviewers:</h3>
			</div>
				<div class="panel-body">
					<form role="form" action="" method="post" id="ConRequest"> 
							<div class="form-group">
								<label for="ConfLongName">Select Reviewer</label>
								<select class="form-control" id="rev" name="rev">
						            <?php 
						            foreach($rowa as $row)
						            { 
						             	echo '<option value="'.$row->Name.'">'.$row->Name.'</option>';
						            }
						            ?>
						        </select>
						        <br>

						        <label for="ConfLongName">Select Paper</label>
								<select class="form-control" id="paper" name="paper">
						            <?php 
						            foreach($groups as $row)
						            { 
						            	 echo '<option value="'.$row->Abstract.'">'.$row->Abstract.'</option>';
						              
						            }
						            ?>
						        </select>
								
								
							</div>
							<input type="submit" class="btn btn-large btn-primary" id="submit" name="submit" value="Assign"> 
							</form>

				</div>
		</div>
		

		

</div> <!-- end of main container -->
<script>
	$('.dropdown-toggle').dropdown()
</script>
<?php include_once('footer.php'); ?>