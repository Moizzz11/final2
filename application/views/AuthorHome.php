<?php include_once('header.php'); ?>
<body>
<div class="container">
  <div class="page-header">
    <center>
      <h1>SLIIT Conference Management System</h1>
      <small>Author Dashboard</small>
    </center>
    
    <!-- Navbar Start!-->
    <nav class="navbar navbar-inverse" role="navigation">
    <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="active"><a href="AuthorHomeC">Author Home</a></li>
   
        <li><a href="UploadPaperC">Submit Paper</a></li>
              
      </ul>
    
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">More Options <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="LoginC/logout">Logout</a></li>
            
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
    <!-- Navbar End!-->
    
    <form role="form" action="AuthorHome.php" method="post">
        
  </form>
  <br>
  <?php
	$this->load->model('UploadPaperM');
	$AuthorID = $this->session->userdata('AuthorID');
	$tmp=$this->UploadPaperM->canAuthorUploadAPaper($AuthorID);
	$dueDate=$this->UploadPaperM->getDueDate();
	if($tmp=='init' || $tmp=='editable'){
		echo '<div class="alert alert-info">';
		echo "<h3>Your Paper submission duedate is $dueDate.</h3>";
		echo '</div>';
	}
	else if($tmp=='noFileDueDatePassed'){
		echo '<div class="alert alert-danger">';
		echo "<h3>Your submission duedate($dueDate) has passed !</h3>";
		echo '</div>';
	}
	else if($tmp=='noneditable'){
		echo '<div class="alert alert-success">';
		echo "<h3>Your paper has been reviewed !</h3>";
		echo '</div>';
	}
  //view other files
	$query = $this->UploadPaperM->getMyOtherPapers($AuthorID);
	echo '<div class="alert alert-info">';
	foreach($query->result() as $row){
		$fileName= $row->fileName;
		$name = $row->Name;
		$email = $row->Email;
		$abstract = $row->Abstract;
		$fileID = $row->FileID;
		$Authors ='';
		$tmp = $this->UploadPaperM->getFileAuthors($fileID);
		foreach($tmp->result() as $a){
			$Authors = $Authors  . $this->UploadPaperM->getAuthorName($a->AuthorID). "</br>";
		}
		echo "<hr><b>Uploaded by :</b><br>$name($email)</br></br><b>All Authors:</b></br>$Authors</br><b>Abstract:</b></br>$abstract</br><a href=\"$fileName\"><img src=\"../images/icon.png\" width=\"50px\" height =\"50px\">Download File</img></a></br>";
	}
	echo '</div>';
	
  ?>
  
  
</div> <!-- end of main container -->
<?php include_once('footer.php'); ?>