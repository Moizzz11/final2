<?php include_once('header.php'); ?>

<body>
<div class="container">
	<div class="page-header">
		<center>
			<h1>SLIIT Conference Management System</h1>
			<small>Reviewer Dashboard</small>
		</center>
		
		<!-- Navbar Start!-->
		<nav class="navbar navbar-inverse" role="navigation">
		<div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="RevDashC">Reviewer Home</a>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li class="dropdown">
          		<a href="#" class="dropdown-toggle" data-toggle="dropdown">My Profile<b class="caret"></b></a>
			         <ul class="dropdown-menu">
			            <li><a href="RevUpdateC">Update</a>
		          	</ul>
        		</li>
						
			</ul>
		
		  <ul class="nav navbar-nav navbar-right">
			<li class="dropdown">
			  <a href="#" class="dropdown-toggle" data-toggle="dropdown">More Options <b class="caret"></b></a>
			  <ul class="dropdown-menu">
				<li><a href="LoginC/logout">Logout</a></li>
				
			  </ul>
			</li>
		  </ul>
		</div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
		<!-- Navbar End!-->

		<?php if(validation_errors()):?>
         	<div class="alert alert-danger">
               	<?php echo validation_errors(); ?>
            </div>
        <?php endif;?>

		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Your current details:</h3>
			</div>
				<div class="panel-body">
				<?php if ($row){ ?>
					<table class="table table-striped">
			            <thead>
			 
			                    <tr>
			                                <th>Name</th>
			                                <th>Email</th>
			                                <th>Specialisation</th>
			                    </tr>
			            </thead>

			         	<?php foreach($row as $result)
			             { ?>
			              	<tr>
			                                    <td><?php echo $result->Name;?></td>
			                                    <td><?php echo $result->Email;?></td>
			                                    <td><?php echo $result->Spec;?></td>

			                </tr>
			            <?php }?>
            	<?php }?>
        			</table>

        			<?php 
                                             if (!$row) {
                                        echo 'No data to display';
                                         } ?>
				</div>
		</div>

		<form role="form" action="" method="post" id="RevUpdate"> 
		<div class="form-group">
			<label for="Name">Update Name</label>
			<input type="text" class="form-control" id="Name" name="Name" placeholder="Enter Name">
		</div>
		<div class="form-group">
			<label for="spec">Update Specialisation</label>
			<input type="text" class="form-control" id="spec" name="spec" placeholder="Enter Specialisation">
		</div>
		<input type="submit" class="btn btn-large btn-primary" id="update" name="update" value="Update"> 
	</form>
	
</div> <!-- end of main container -->
<script>
	$('.dropdown-toggle').dropdown()
</script>
<?php include_once('footer.php'); ?>