<?php include_once('header.php'); ?>

<body>
<div class="container">
 <div class="page-header">
   <center>
     <h1>SLIIT Conference Management System</h1>
     
     
     
   </center>
 </div>

   

   <div class="container">
       <?php if(validation_errors()):?>
                                   <div class="alert alert-danger">
                                       <?php echo validation_errors(); ?>
                                   </div>
                                     <?php endif;?>
                                     <?php echo $this->session->flashdata('feedback');?>
        <?php echo form_open('AuthorSignupC');?>
     
       <div class="form-signin">
       <h4 class="form-signin-heading">Sign up for a conference here!</h4><br>
       Select a conference:
       <select class="form-control" id="ConfName" name="ConfName">
            <?php 
            foreach($groups as $row)
            { 
              echo '<option value="'.$row->ConfLName.'">'.$row->ConfLName.'</option>';
            }
            ?>
        </select>
        <br>
       <input type="text" class="input-block-level" placeholder="Name" id="name" name="name" value="<?php echo set_value('name'); ?>" />
       <input type="text" class="input-block-level" placeholder="Email address" id="email" name="email" value="<?php echo set_value('email'); ?>" />
       <input type="password" class="input-block-level" placeholder="Password" id="pw" name="pw" value="<?php echo set_value('pw'); ?>"/>
       <input type="password" class="input-block-level" placeholder="Confirm Password" id="cpw" name="cpw" value="<?php echo set_value('cpw'); ?>"/>
       <input type="text" class="input-block-level" placeholder="Contact Number" id="num"  name="num" value="<?php echo set_value('num'); ?>"/>
       <input type="submit" class="btn btn-large btn-primary" id="submit" name="submit" value="Sign Up!">
       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="LoginC">Log In</a> 

     </div>
   </div>

<?php include_once('footer.php'); ?>