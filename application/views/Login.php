<?php include_once('header1.php'); ?>

<body>

			<div class="container">
				<div class="page-header">
					<center>
						<h1 style="color:#00BFFF;">SLIIT Conference Management System</h1>
						<small style="color:white;">This Conference Management Toolkit is a free conference management service sponsored by SLIIT. This application is capable of handling the complex workflow of an academic conference.</small>
					</center>
				</div>

					

					<div class="container">
							<?php if(validation_errors()):?>
			                                    <div class="alert alert-danger">
			                                        <?php echo validation_errors(); ?>
			                                    </div>
			                                      <?php endif;?>
			             <?php echo form_open('LoginC');?>
						
							<div class="form-signin">
							<h2 class="form-signin-heading">Please sign in</h2>
							<input type="text" class="input-block-level" placeholder="Email address" id="Email" name="Email">
							<input type="password" class="input-block-level" placeholder="Password" id="Password" name="Password">
						<button class="btn btn-large btn-primary" type="submit">Sign in</button><br><br>
						<a href="AuthorSignupC">Register for a conference</a> 
						</div>
					</div>
					
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">Interested in creating a conference?</h3>
						</div>
							<div class="panel-body">
								<center><a href="ConRequestC"> Click here to submit a new conference site request. </a> </center>
							</div>
					</div>
			</div> <!-- end of main container -->



<?php include_once('footer.php'); ?>