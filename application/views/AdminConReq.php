<?php include_once('header.php'); ?>
<body>
<div class="container">
	<div class="page-header">
		<center>
			<h1>SLIIT Conference Management System</h1>
			<small>Administrator Dasboard</small>
		</center>
		
		<!-- Navbar Start!-->
		<nav class="navbar navbar-inverse" role="navigation">
		<div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="AdminDashC">Admin Home</a>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li class="active"><a href="AdminConReqC">Create Conference</a></li>
				<li><a href="AdminAllConC">All Conferences</a></li>
				<li><a href="AdminAllUsersC">ChairPersons</a></li>
				<li><a href="AdminUsersC">All Users</a></li>
				<li><a href="AdminEmailC">Email Details</a></li>			
			</ul>
		
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">More Options <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="LoginC/logoutAdmin">Logout</a></li>
            
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
		<!-- Navbar End!-->


		<!-- <div class="alert alert-info">Test</div>
		 <?php echo validation_errors(); ?> -->

		<?php if(validation_errors()):?>
         	<div class="alert alert-danger">
               	<?php echo validation_errors(); ?>
            </div>
        <?php endif;?>

		<form role="form" action="" method="post">
		<div class="form-group">
			<label for="ConfLongName">Conference Name?</label>
			<input type="text" class="form-control" id="ConfLongName" name="ConfLongName" placeholder="Enter Name">
		</div>
		<div class="form-group">
			<label for="ConfShortName">Conference Short Name?</label>
			<input type="text" class="form-control" id="ConfShortName" name="ConfShortName" placeholder="Enter Short Name">
		</div>
		<div class="form-group">
			<label for="SubDate">Submission due date?</label>
			<input type="text" class="form-control" id="SubDate" name="SubDate" placeholder="DD/MM/YYYY">
		</div>
		<div class="form-group">
			<label for="Subject">Conference subject area?</label>
			<input type="text" class="form-control" id="Subject" name="Subject" placeholder="Enter a brief description">
		</div>

		<input type="submit" class="btn btn-large btn-primary" id="submit" name="submit" value="Submit">
		
	</form>
	<br>
		<a href="AddChairC">Add Chairperson</a>
	
</div> <!-- end of main container -->
<?php include_once('footer.php'); ?>