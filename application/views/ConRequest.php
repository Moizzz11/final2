<?php include_once('header.php'); ?>

<body>
<div class="container">
	<div class="page-header">
		<center>
			<h1>SLIIT Conference Management System</h1>
			<small>New Conference Submission Site Request</small>
		</center>
	</div>

	<?php if(validation_errors()):?>
         	<div class="alert alert-danger">
               	<?php echo validation_errors(); ?>
            </div>
        <?php endif;?>

	<form role="form" action="" method="post" id="ConRequest">
		<div class="form-group">
			<label for="ConfLongName">What is the name of your conference?</label>
			<input type="text" class="form-control" id="ConfLongName" name="ConfLongName" placeholder="Enter Name">
		</div>
		<div class="form-group">
			<label for="ConfShortName">What is a short name for your conference?</label>
			<input type="text" class="form-control" id="ConfShortName" name="ConfShortName" placeholder="Enter Short Name">
		</div>
		<div class="form-group">
			<label for="SubDate">What is the paper submission due date?</label>
			<input type="text" class="form-control" id="SubDate" name="SubDate" placeholder="YYYY/MM/DD">
		</div>

		<!-- <div class="form-group">
			
			<input id="datetimepicker1" type="text">
		</div> -->

		<div class="form-group">
			<label for="Tracks">Please provide a list of Track Titles for your conference</label>
			<input type="text" class="form-control" id="Tracks" name="Tracks" placeholder="Seperate each with a comma">
		</div>
		<div class="form-group">
			<label for="Subject">What is your conference subject area?</label>
			<input type="text" class="form-control" id="Subject" name="Subject" placeholder="Enter a brief description">
		</div>
		
		<input type="submit" class="btn btn-large btn-primary" id="update" name="update" value="Update"> &nbsp;
		<a href="LoginC">Back</a>
		
	</form>	
	<br>	
</div> <!-- end of main container -->

<?php include_once('footer.php'); ?>