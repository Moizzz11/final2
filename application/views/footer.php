<!-- javascript -->
	<script src="http://code.jquery.com/jquery-latest.min.js"></script>
    <script src="../bootstrap/js/bootstrap.min.js"></script>
    <script>
		$('a.btn-info').tooltip()
	</script>
</body>

<link rel="stylesheet" type="text/css" href="../bootstrap/css/jquery.datetimepicker.css"/ >
<script src="../bootstrap/js/jquery.js"></script>
<script src="../bootstrap/js/jquery.datetimepicker.js"></script>
<script type="text/javascript">
	jQuery('#SubDate').datetimepicker({
 lang:'de',
 i18n:{
  de:{
   months:[
    'January','February','March','April',
    'May','June','July','August',
    'September','October','November','December',
   ],
   dayOfWeek:[
    "Sun.", "Mon", "Tue", "Wed", 
    "Thur", "Fri", "Sat.",
   ]
  }
 },
 timepicker:false,
 format:'Y.m.d'
});
</script>
</html>