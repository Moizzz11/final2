<?php
session_start();
class LoginC extends CI_Controller{
	public function index()
	{
		//$this->load->view('Login');
		$this->load->library('form_validation');

   		$this->form_validation->set_rules('Email', 'Email', 'trim|required|xss_clean');
   		$this->form_validation->set_rules('Password', 'Password', 'trim|required|xss_clean|callback_checkDB');

   		if($this->form_validation->run() == FALSE)
   		{
     		//Field validation failed.&nbsp; User redirected to login page
     		$this->load->view('Login');
   		}
  
      	else
      	{
     		redirect('TypesC', 'refresh');
     	}
	}


	function checkDB()
 	{
	   //Field validation succeeded.&nbsp; Validate against database
	   //$Email= $this->input->post('Email');
	   //$PW = $this->input->post('Password');

	   //query the database
 		$this->load->model('LoginM');
 		$this->load->model('UploadPaperM');
	   $result = $this->LoginM->login();

	   if($result)
	   {
	     $sess_array = array();
	     foreach($result as $row)
	     {
	       $sess_array = array(
	         'Email' =>$row->Email,
	         'Type'=>$row->Type,
	         'ConfID'=>$row->ConfID,
	         'ID'=>$row->ID,
	         'ConfLName'=>$row->ConfLName);
	          $this->session->set_userdata('LoggedIn', $sess_array);

	          if($row->Type=='A')
	          {
				$this->session->set_userdata('AuthorID' , $this->UploadPaperM->getAuthorID($row->Email,$row->ConfID));
			  }

	     }
	     return TRUE;
	   }
	   else
	   {
	     $this->form_validation->set_message('checkDB', 'Invalid username or password');
	     return false;
	   }
	}

	function logout()
 	{
 		//$this->unset_session();
   		$this->session->unset_userdata('LoggedIn');
   		session_destroy();
   		redirect('LoginC', 'refresh');
 	}

 	function logoutAdmin()
 	{
   		$this->session->unset_userdata('LoggedIn');
   		session_destroy();
   		redirect('AdminLoginC', 'refresh');
 	}
}

?>