<?php
class AddChairC extends CI_Controller{
	public function index()
	{
		if($this->input->post('submit'))
        {
        	$this->load->helper(array('form', 'url'));
        	$this->load->library('email');
			$this->load->library('form_validation');
			$this->form_validation->set_rules('ConfID','Conference ID','required|callback_CheckChair');
	        $this->form_validation->set_rules('ConfChairName','Chairperson Name','required');
	        $this->form_validation->set_rules('ConfChairEmail','Chairperson Email','required');

	        if ($this->form_validation->run()===FALSE)
            {
                
            }

             else
            {
            	$this->load->model('AddRevM');
                $res = $this->AddRevM->AddChair();
                $email = $this->input->post('ConfChairEmail');
                $pw = $this->AddRevM->RetPW();
                $data = "Congratulations! Your request for a conference has been approved. Your login details are:
                		 Email: $email
                		 Password: $pw";

                if ($res)
                {
                	echo '<div class="alert alert-success" <a class="close pull-right" data-dismiss="alert"></a>Successfully added!</div>';
                	$this->email->from('admin@sliitcmt.com', 'SLIIT CMT');
                	$this->email->to($email); 

                	$this->email->subject('Request for Conference');
                	$this->email->message($data);  

                	$this->email->send();


                }
            }
        }

        $this->load->view('AddChair');
	}

	public function CheckChair()
	{
		$this->load->model('AddRevM');
		$result = $this->AddRevM->CheckChairExist();

		if($result)
	   	{
			return TRUE;	
		}

		else
		{
			$this->form_validation->set_message('CheckChair', 'Chairperson has already been added to this conference!');
			return FALSE;
		}	
	} 
}
?>