<?php
class AdminLoginC extends CI_Controller{
	public function index()
	{

		$this->load->library('form_validation');
   		$this->form_validation->set_rules('un', 'Email', 'trim|required|xss_clean');
   		$this->form_validation->set_rules('pw', 'Password', 'trim|required|xss_clean|callback_checkDBadmin');


   		if($this->form_validation->run() == FALSE)
   		{
     		//Field validation failed.&nbsp; User redirected to login page
     		//$this->load->view('Login');
   		}
  
      	else
      	{
     		redirect('AdminC', 'refresh');
     	}

		$this->load->view('AdminLogin');
	}



	function checkDBadmin()
 	{
	  
 		$this->load->model('LoginM');
	    $result = $this->LoginM->loginAdmin();

	   if($result)
	   {
	     $sess_array = array();
	     foreach($result as $row)
	     {
	       $sess_array = array(
	         'AdminID' =>$row->AdminID,
	         'AdminUN'=>$row->AdminUN,
	         'AdminName'=>$row->AdminName);
	       	session_start();
	          $this->session->set_userdata('LoggedIn', $sess_array);
	     }
	     return TRUE;
	   }
	   else
	   {
	     $this->form_validation->set_message('checkDBadmin', 'Invalid username or password');
	     return false;
	   }
	}
}
?>