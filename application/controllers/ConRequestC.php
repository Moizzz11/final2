<?php
class ConRequestC extends CI_Controller
{
	public function index()
	{
        
        $this->load->helper(array('form', 'url'));
        $this->load->library('email');

		if($this->input->post('update'))
        {

            $this->load->library('form_validation');
            $this->form_validation->set_rules('ConfLongName', 'Name', 'required');
            $this->form_validation->set_rules('ConfShortName', 'Shortname', 'required');
            $this->form_validation->set_rules('SubDate', 'Due Date', 'required');
            $this->form_validation->set_rules('Tracks', 'Track Titles', 'required');
            $this->form_validation->set_rules('Subject', 'Subject Area', 'required');
            
            $ConfLongName = $this->input->post('ConfLongName');
            $ConfShortName = $this->input->post('ConfShortName');
            $SubDate = $this->input->post('SubDate');
            $Tracks = $this->input->post('Tracks');
            $Subject = $this->input->post('Subject');

            $data = "Conference Long Name: $ConfLongName
                     Conference Short Name: $ConfShortName 
                     Paper Submission Due Date: $SubDate
                     Tracks: $Tracks
                     Subject: $Subject";

            if ($this->form_validation->run()===FALSE)
            {
                

            }

             else
            {
                $this->email->from('conrequest@sliitcmt', 'ConferenceRequest');
                $this->email->to('admin@sliitcmt.com'); 

                $this->email->subject('Request for Conference');
                $this->email->message($data);  

                $this->email->send();
            }
        }

        $this->load->view('ConRequest');
	}
}
?>