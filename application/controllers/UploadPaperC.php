<?php
class UploadPaperC extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
	}

	function index()
	{
		$this->load->model('UploadPaperM');
		session_start();
		$AuthorID = $this->session->userdata('AuthorID');
		$res = $this->UploadPaperM->canAuthorUploadAPaper($AuthorID);
		if($res=='init'){
			$this->load->view('UploadPaper', array('upload' => ' ' ));
		}
/*		else if($res=='editable'){
			$this->load->view('UploadPaper', array('viewFile' => 'editable' ));
		} */
		else{
			//echo ">>res>>>$res<<<";
			$this->load->view('UploadPaper', array('viewFile' => $res ));
		}
/*		else{
			$this->load->view('UploadPaper', array('viewFile' => '' ));
		} */
//	echo ">>>$res<<<";
	}

	function do_upload()
	{
		$config['upload_path'] = './uploads/';
		$config['allowed_types'] = 'pdf|PDF|doc|DOC|docx|DOCX';
		$config['max_size']	= '10000';
		$config['file_name'] = $this->session->userdata('AuthorID') . '_' . md5(uniqid(rand(), true));
		
		$this->load->library('upload', $config);
		$this->upload->overwrite = true;
		$AuthorID = $this->session->userdata('AuthorID');
		$this->load->model('UploadPaperM');
		$res = $this->UploadPaperM->canAuthorUploadAPaper($AuthorID);
		if( isset($_POST['Track']) ){
			$Track = $_POST['Track'];
			$Abstract = $_POST['abstract'];
		}				
		if (!empty($_FILES['userfile']['name'])){
			if ( ! $this->upload->do_upload())
			{
				$error = array('error' => $this->upload->display_errors());

				$this->load->view('UploadPaper', $error);
			}
			
			else
			{
				
				$data = array('upload_data' => $this->upload->data());
				//$this->index();
				
				$this->load->model('UploadPaperM');
				$a = $this->UploadPaperM->uploadPaperToDB($data, $Track, $Abstract);
				
				
				
					$this->load->view('UploadPaper',$data);
				
				//$tmp = array('d' => $a);
				//$this->load->view('test', $tmp);
					
				
			}	
		}else 
		if($res =='init'){
					$error = array('error' => 'No file selected ! </br>');
					$this->load->view('UploadPaper', $error);
				}
		else{
			$data ='';
			$this->load->model('UploadPaperM');
				
			$a = $this->UploadPaperM->uploadPaperToDB($data, $Track, $Abstract);
			$successMessage = array('successMessage' => 'Paper updated !');
			$this->load->view('UploadPaper', $successMessage);
		}
		
/**/	
	}
	
	function add_author(){
		$AuthorID = $this->session->userdata('AuthorID');
		$tmp = $this->session->userdata('LoggedIn');
		$confID = $tmp['ConfID'];
		$authorEmailtoAdd = $_POST['authorSelect'];
		
		$this->load->model('UploadPaperM');
		$newAuthorID = $this->UploadPaperM->getAuthorID($authorEmailtoAdd, $confID);
		//echo "newID>>>$newAuthorID<<<";
		if($newAuthorID != false){
			$data1= array('FileID'=>$AuthorID ,'AuthorID'=>$newAuthorID);
			$this->db->insert('file_owned_by',$data1);
			$this->index();
		}
		else{
			$error = array('error' => 'Invalid email address, please type the email address and try again ! </br>');
			$this->load->view('UploadPaper', $error);
			//echo '......' . $newAuthorID . '<<<';
			//$this->index();
		}
	}
	
}
?>