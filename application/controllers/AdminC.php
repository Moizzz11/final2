<?php
//session_start();
class AdminC extends CI_Controller
{
    public function index()
    {
        
        //session_start();
        if($this->session->userdata('LoggedIn'))
        {
            $session_data = $this->session->userdata('LoggedIn');
            $data=array();
            $data['AdminID'] = $session_data['AdminID']; 
            $data['AdminName']= $session_data['AdminName'];

            $this->load->view('AdminDash', $data);
         }

          else
        {
             //If no session, redirect to login page
            redirect('AdminLoginC', 'refresh');
        }
    }
}

?>