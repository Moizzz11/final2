<?php
class AdminUsersC extends CI_Controller{
	public function index()
	{

		$this->load->model('AdminUsersM');
		$data['rowc']=$this->AdminUsersM->ViewChairUsers();
		$data['rowa']=$this->AdminUsersM->ViewAuthorUsers();
		$data['rowr']=$this->AdminUsersM->ViewReviewUsers();
		$this->load->view('AdminUsers',$data);

	}

	
	public function deleteChair($ChairID)
	{
		$this->load->model('AdminUsersM');
		
		if((int)$ChairID > 0)
		{
		$this->AdminUsersM->deleteChair($ChairID);
		}
		redirect('AdminUsersC');
	}
	
		public function deleteAuthors($AuthorID)
	{
		$this->load->model('AdminUsersM');
		
		if((int)$AuthorID > 0)
		{
		$this->AdminUsersM->deleteAuthors($AuthorID);
		}
		redirect('AdminUsersC');
	}

	public function deleteReview($RID)
	{
		$this->load->model('AdminUsersM');
		
		if((int)$RID > 0)
		{
		$this->AdminUsersM->deleteReview($RID);
		}
		redirect('AdminUsersC');
	}
}

?>