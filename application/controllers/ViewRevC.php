<?php
class ViewRevC extends CI_Controller
{
	public function index()
	{
		

		$this->load->model('AddRevM');
		$data['row']=$this->AddRevM->ViewRev();
		$this->load->view('ViewRev',$data);
	}

	public function deleteValue($RevID)
	{
		$this->load->model('AddRevM');
		$this->AddRevM->DeleteRev($RevID);
		redirect('ViewRevC');
	}

}
?>