<?php
class AddComC extends CI_Controller
{
	public function index()
	{
		if($this->input->post('submit'))
        {
        	$this->load->helper(array('form', 'url'));
        	$this->load->library('email');
			$this->load->library('form_validation');
			$this->form_validation->set_rules('ComName','Committee Name','required');
	        $this->form_validation->set_rules('ComHeadName','Committee Head Name','required');
	        $this->form_validation->set_rules('Email','Committee Head Email','required|callback_CheckCH');

	        if ($this->form_validation->run()===FALSE)
            {
                
            }

             else
            {
            	$this->load->model('AddRevM');
                $this->AddRevM->AddComm();

                $email = $this->input->post('Email');
                $pw = $this->AddRevM->RetPWCH();
                $name = $this->AddRevM->RetConName();
                $comname = $this->input->post('ComName');

                $data = "Congratulations! You have been appointed as a Committee Head for the committee: $comname in the conference: $name .    Your login details are:
                         Email: $email
                         Password: $pw";
                echo '<div class="alert alert-success" <a class="close pull-right" data-dismiss="alert"></a>Successfully added!</div>';
                $this->email->from('admin@sliitcmt.com', 'SLIIT CMT');
                $this->email->to($email); 

                $this->email->subject('Committee Head Appointment');
                $this->email->message($data);  

                $this->email->send();
            }
        }
		
		$this->load->view('AddCom');
	}

    public function CheckCH()
    {
        $this->load->model('ComM');
        $result = $this->ComM->CheckCHExist();

        if($result)
        {
            return TRUE;    
        }

        else
        {
            $this->form_validation->set_message('CheckCH', 'Committee Head with same email has already been added to this conference!');
            return FALSE;
        }   
    } 
}
?>