<?php
class AddTracksC extends CI_Controller{
	public function index()
	{
		$this->load->library('email');
        $this->load->library('form_validation');
        $this->load->model('AddRevM');
		$data['row']=$this->AddRevM->ViewTracks();
        $this->session->set_flashdata("Successfully added!");

        if($this->input->post('submit'))
        {
             $this->form_validation->set_rules('Name','Track','required|callback_CheckTrack');
            

             if ($this->form_validation->run()===FALSE)
            {
                
            }

             else
            {
            	
                $res = $this->AddRevM->AddTracks();
                redirect('AddTracksC');
                if ($res)
                {
                    $this->session->flashdata();
                    //echo '<div class="alert alert-success" <a class="close pull-right" data-dismiss="alert"></a>Successfully added!</div>';
                }
            }
        }

		$this->load->view('AddTracks',$data);
	}

	function CheckTrack()
	{
		$this->load->model('AddRevM');
        $result = $this->AddRevM->CheckTracks();

        if($result)
        {
            return TRUE;    
        }

        else
        {
            $this->form_validation->set_message('CheckTrack', 'This track has already been added to this conference!');
            return FALSE;
        }
	}


    public function deleteValue($TrackID)
    {
        $this->load->model('AddRevM');
        $this->AddRevM->DeleteTracks($TrackID);
        redirect('AddTracksC');
    }
}
?>