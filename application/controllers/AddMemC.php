<?php
class AddMemC extends CI_Controller
{
	public function index()
	{
		$this->load->library('email');
        $this->load->library('form_validation');
        $this->load->model('ComM');
        $this->load->model('AddRevM');
		$data['row']=$this->ComM->ViewMem();

        if($this->input->post('submit'))
        {
             $this->form_validation->set_rules('MemName','Member Name','required');
             $this->form_validation->set_rules('Email','Member Email','required|callback_CheckMem');

             if ($this->form_validation->run()===FALSE)
            {
                
            }

             else
            {
                $this->ComM->AddMem();

                $email = $this->input->post('Email');
                $pw = $this->ComM->RetPWCM();
                $name = $this->AddRevM->RetConName();
                $comname = $this->ComM->RetCommName();

                $data = "Congratulations! You have been appointed as a Committee Member for the committee: $comname in the conference:$name. Your login details are:
                      Email: $email
                      Password: $pw";

                $this->email->from('admin@sliitcmt.com', 'SLIIT CMT');
                $this->email->to($email); 

                $this->email->subject('Committee Member Appointment');
                $this->email->message($data);  

                $this->email->send();

                redirect('AddMemC');

                echo '<div class="alert alert-success" <a class="close pull-right" data-dismiss="alert"></a>Successfully added!</div>';
            }
        }

		$this->load->view('AddMem',$data);
	}


	public function CheckMem()
    {
        $this->load->model('ComM');
        $result = $this->ComM->CheckMemExist();

        if($result)
        {
            return TRUE;    
        }

        else
        {
            $this->form_validation->set_message('CheckMem', 'Member has already been added to this same committee!');
            return FALSE;
        }   
    }
}
?>