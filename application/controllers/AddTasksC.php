<?php
class AddTasksC extends CI_Controller
{
	public function index()
	{
		$this->load->model('ComM');
		$this->load->helper(array('form', 'url'));
        $this->load->library('email');
		$this->load->library('form_validation');
        $data['row']=$this->ComM->ViewTasks();

		if($this->input->post('task1'))
        {
        	
			$this->form_validation->set_rules('1task','Task Name','required');
            $this->form_validation->set_rules('desc','Task Description','required');

	        if ($this->form_validation->run()===FALSE)
            {
                
            }

             else
            {
            	$task1 = $this->input->post('1task');
            	$this->ComM->AddTask1();
                redirect('AddTasksC');
            	echo '<div class="alert alert-success" <a class="close pull-right" data-dismiss="alert"></a>Successfully added!</div>';
            }
        }

		$this->load->view('AddTasks',$data);
	}

    public function deleteValue($TaskID)
    {
        $this->load->model('ComM');
        $this->ComM->DeleteTasks($TaskID);
        redirect('AddTasksC');
    }
}