<?php
class AddRevC extends CI_Controller
{
	function index()
	{
        $this->load->library('email');
        $this->load->library('form_validation');

        if($this->input->post('submit'))
        {
             $this->form_validation->set_rules('Email','Email','required|callback_CheckRev');

             if ($this->form_validation->run()===FALSE)
             {
                
             }

             else
            {
                $this->load->model('AddRevM');
                $this->AddRevM->AddRev();
                $name = $this->AddRevM->RetConName();
                $email = $this->input->post('Email');
                $pw = $this->AddRevM->RetPWRev();
                $data = "Congratulations! You have been appointed as a reviewer for the conference: $name . Your login details are:
                         Email: $email
                         Password: $pw";
                echo '<div class="alert alert-success" <a class="close pull-right" data-dismiss="alert"></a>Successfully added!</div>';
                $this->email->from('admin@sliitcmt.com', 'SLIIT CMT');
                $this->email->to($email); 

                $this->email->subject('Reviewer Appointment');
                $this->email->message($data);  

                $this->email->send();
            }
        }

        $this->load->view('AddRev');       
    }

    public function CheckRev()
    {
        $this->load->model('AddRevM');
        $result = $this->AddRevM->CheckRevExist();

        if($result)
        {
            return TRUE;    
        }

        else
        {
            $this->form_validation->set_message('CheckRev', 'Reviewer has already been added to this same conference!');
            return FALSE;
        }   
    }   
}

?>