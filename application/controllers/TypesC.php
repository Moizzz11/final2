<?php
session_start();
class TypesC extends CI_Controller
{
    public function index()
    {
        $this->load->model('UploadPaperM');
        
        if($this->session->userdata('LoggedIn'))
        {
             $session_data = $this->session->userdata('LoggedIn');
             $data=array();
             $data['Email'] = $session_data['Email']; 
             $data['ConfLName']= $session_data['ConfLName'];
             $data['ID']= $session_data['ID'];
               
             $UserType=$session_data['Type'];
             if ($UserType=='CP')
             {
                $this->load->view('ChairDash', $data);
             }
             elseif($UserType=='R')
             {
                $this->load->view('RevDash',$data);
             }
             elseif($UserType=='CH')
             {
                $this->load->model('ComM');
                $data['a'] = $this->ComM->RetCommName();
                
                $this->load->view('ComHDash',$data);
             }
             elseif($UserType=='A')
             {
                $this->load->view('AuthorHome',$data);   
             }
             elseif($UserType=='CM')
             {
                $this->load->view('ComMDash',$data);   
             }
             else
             {
             //If no session, redirect to login page
                redirect('LoginC', 'refresh');
             }
         }
    }
}

?>