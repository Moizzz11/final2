<?php
class RevUpdateC extends CI_Controller
{
	public function index()
	{
        $this->load->library('form_validation');
		$this->load->model('AddRevM');
		$data['row']=$this->AddRevM->ViewRevOnly();
		

		if($this->input->post('update'))
        {
             
             //$this->load->model('AddRevM');
             
             
             $this->form_validation->set_rules('Name','Name','required');
             $this->form_validation->set_rules('spec','Specialisation','required');
             if ($this->form_validation->run()===FALSE)
             {
                
                    //$this->load->view('RevUpdate');
             }

             else
            {
                $this->load->model('AddRevM');
                $this->AddRevM->ModRev();
                redirect('RevUpdateC');
            }
        }

        $this->load->view('RevUpdate',$data);    
        
    }   

}

?>