<?php
class AuthorSignupC extends CI_Controller
{
	function index()
	{
			$this->load->model('AuthorSignupM');
			$this->load->helper(array('form', 'url'));
			$this->load->library('form_validation');
			$data['title']= 'All Conferences';
        	$data['groups'] = $this->AuthorSignupM->ViewAllConf();

        	if($this->input->post('submit'))
        	{

        		$this->form_validation->set_rules('ConfName', 'Conference', 'required');
				$this->form_validation->set_rules('email', 'Email', 'required|callback_CheckDup');
				$this->form_validation->set_rules('pw', 'Password', 'required|matches[cpw]');
				$this->form_validation->set_rules('cpw', 'Password Confirmation', 'required');
				$this->form_validation->set_rules('num','Contact Number','required');


				if($this->form_validation->run() == FALSE)
				{
					
				}
		 
				else
				{
		
			   		$result=$this->AuthorSignupM->signup();	   		
			   		if($result)
			   		{
			   			//echo '<div class="alert alert-success" <a class="close pull-right" data-dismiss="alert"></a>Successfully added!</div>';	
			   			$this->session->set_flashdata('feedback', '<div class="alert alert-success" <a class="close pull-right" data-dismiss="alert"></a>Sucsessfully Registered!</div>');
			   			redirect('AuthorSignupC');
			   		}
			   		
				}
			}

			$this->load->view('AuthorSignup', $data);
	}

	public function registerAuthor()
	{
		$res = FALSE;
		$this->load->model('AuthorSignupM');
		$res = $this->AuthorSignupM->signup();	  

		if ($res==FALSE) 
		{
			$this->form_validation->set_message('registerAuthor', 'Registration Failed');
		}
		return $res;
	}

	public function CheckDup()
	{
		$this->load->model('AuthorSignupM');
        $result = $this->AuthorSignupM->CheckAExist();

        if($result)
        {
            return TRUE;    
        }

        else
        {
            $this->form_validation->set_message('CheckDup', 'An Author with this email has already been added to this same conference! Please try with a new one!');
            return FALSE;
        }
	}
}

?>