<?php
class AdminAllUsersC extends CI_Controller{
	public function index()
	{

		$this->load->model('AdminAllUsersM');
		$data['row']=$this->AdminAllUsersM->ViewAllUsers();
		$this->load->view('AdminAllUsers',$data);
		
	}
	public function deleteChair($ChairID)
	{
		$this->load->model('AdminAllUsersM');
		
		if((int)$ChairID > 0)
		{
		$this->AdminAllUsersM->deleteChair($ChairID);
		}
		redirect('AdminAllUsersC');
	}
	

	public function EmailChair($ChairEmail)
	{
		$this->load->model('AdminAllUsersM');
		if((int)$ChairEmail > 0)
		{
			$this->AdminAllUsersM->EmailChair($ChairEmail);
		}

		redirect('AdminEmailChairC');
	
	}
}

?>