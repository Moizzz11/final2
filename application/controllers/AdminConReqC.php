<?php
class AdminConReqC extends CI_Controller
{
	public function index()
	{	

		$this->load->helper(array('form', 'url'));
        $this->load->library('email');
        $this->load->library('form_validation');

		if($this->input->post('submit'))
        {

            
            $this->form_validation->set_rules('ConfLongName', 'Name', 'required');
            $this->form_validation->set_rules('ConfShortName', 'Shortname', 'required');
            $this->form_validation->set_rules('SubDate', 'Due Date', 'required');
            $this->form_validation->set_rules('Subject', 'Subject Area', 'required');

            if ($this->form_validation->run()===FALSE)
            {
                
            }

             else
            {
                $this->load->model('AddRevM');
                $id = $this->AddRevM->AddConf();
                //echo "<center>Conference has been added</center>";
                //echo $id;
                echo '<div class="alert alert-success" <a class="close pull-right" data-dismiss="alert"></a>Conference ID: '.$id.'</div>';
            }
        }
        
		$this->load->view('AdminConReq');
	}
}
?>